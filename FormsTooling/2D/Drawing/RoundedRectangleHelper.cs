﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;




namespace FormsTooling._2D.Drawing
{


        public class RoundedRectHelper
        {
            /// <summary>
            /// Creates a Point array describing an interpolated arc. Used by DrawPolygon/ FillPolygon. 
            /// </summary>
            /// <param name="centerX">mid point X of the arc</param>
            /// <param name="centerY">mid point Y of the arc</param>
            /// <param name="radius">Radius</param>
            /// <returns></returns>
            public static PointF[] CreatePolygonArcPoints(int centerX, int centerY, int radius, double startAngle, double endAngle)
            {
                double deltaAlfa = MaxAngleFromRadius(radius);
                double angleDist = endAngle - startAngle;
                int steps = (int)System.Math.Ceiling(angleDist / deltaAlfa);
                List<PointF> pointList = new List<PointF>();
                for (int i = 0; i <= steps; i++)
                {
                    double radiant = startAngle + ((double)i * angleDist) / (double)steps;
                    float x = (float)(centerX + radius * System.Math.Cos(radiant));
                    float y = (float)(centerY + radius * System.Math.Sin(radiant));

                    int ix = System.Convert.ToInt32(System.Math.Round(x));
                    int iy = System.Convert.ToInt32(System.Math.Round(y));

                    PointF pt = new PointF(x, y);

                    pointList.Add(pt);
                }
                PointF[] rval = pointList.ToArray();
                return rval;

            }

            /// <summary>
            /// Creates a Point array describing an interpolated rectangle with rounded corners. Used by DrawPolygon/ FillPolygon.
            /// </summary>
            /// <param name="rect">Describes Poisition and size of the rectangle</param>
            /// <param name="cornerRadius">Cornerradius as often used</param>
            /// <returns></returns>
            public static PointF[] CreatePolygonRectPoints(Rectangle rect, int cornerRadius)
            {
                double pi = System.Math.PI;
                PointF[] LeftTopCorner = CreatePolygonArcPoints(rect.Left + cornerRadius, rect.Top + cornerRadius, cornerRadius, pi, pi * 3 / 2);
                PointF[] RightTopCorner = CreatePolygonArcPoints(rect.Right - cornerRadius, rect.Top + cornerRadius, cornerRadius, pi * 3 / 2, pi * 2);
                PointF[] RightBottomCorner = CreatePolygonArcPoints(rect.Right - cornerRadius, rect.Bottom - cornerRadius, cornerRadius, 0, pi / 2);
                PointF[] LeftBottomCorner = CreatePolygonArcPoints(rect.Left + cornerRadius, rect.Bottom - cornerRadius, cornerRadius, pi / 2, pi);

                int rvalLength = LeftTopCorner.Length + RightTopCorner.Length + LeftBottomCorner.Length + RightBottomCorner.Length;

                PointF[] retVal = new PointF[rvalLength];
                int rvalIndex = 0;
                LeftTopCorner.CopyTo(retVal, rvalIndex);
                rvalIndex += LeftTopCorner.Length;
                RightTopCorner.CopyTo(retVal, rvalIndex);
                rvalIndex += RightTopCorner.Length;
                RightBottomCorner.CopyTo(retVal, rvalIndex);
                rvalIndex += RightBottomCorner.Length;
                LeftBottomCorner.CopyTo(retVal, rvalIndex);
                rvalIndex += LeftBottomCorner.Length;

                return retVal;



            }


            /// <summary>
            /// How large can be an angle to interpolate an arc with lines (max error 1 pixel)
            /// </summary>
            /// <param name="r">arc radius</param>
            /// <returns>the interpolation angle</returns>
            public static double MaxAngleFromRadius(int r)
            {
                if (r == 0)
                    return System.Math.PI * 2;

                double x = (1 / (2 * (double)r)) - 1;
                double rval = System.Math.Acos(2 * x * x - 1);
                return rval;
            }

        }

}
