﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D.Drawing
{
    /// <summary>
    /// Interface for objects supporting DrawCommands in a DrawContainer
    /// </summary>
    public interface IDrawObject
    {
        List<IDrawCommand> DrawCommands{get; }
    }

    /// <summary>
    /// Interface for a draw command called by DrawContainer.OnPaint
    /// </summary>
    public interface IDrawCommand
    {
        void Draw(double offsetX, double offsetY, double zoom, System.Drawing.Graphics g); 
    }


    /// <summary>
    /// DrawCommand: Draws a line
    /// </summary>
    public class DrawLine : IDrawCommand
    {
        public DrawLine(PointCoordinate p1, PointCoordinate p2, System.Drawing.Color color)
        {
            P1 = p1;
            P2 = p2;
            Color = color;
        }

        public DrawLine(PointCoordinate p1, PointCoordinate p2, System.Drawing.Color color, int width)
        {
            P1 = p1;
            P2 = p2;
            Color = color;
            Width = width;
        }

        
        
        public PointCoordinate P1;
        public PointCoordinate P2;
        public System.Drawing.Color Color;
        public int Width = 1;

        public void Draw(double offsetX, double offsetY, double zoom, System.Drawing.Graphics g)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color);
            pen.Width = Width;
            System.Drawing.PointF P_1 = new System.Drawing.PointF(System.Convert.ToSingle(P1.X * zoom + offsetX), System.Convert.ToSingle(P1.Y * zoom + offsetY));
            System.Drawing.PointF P_2 = new System.Drawing.PointF(System.Convert.ToSingle(P2.X * zoom + offsetX), System.Convert.ToSingle(P2.Y * zoom + offsetY)); 
            g.DrawLine(pen, P_1, P_2);
            pen.Dispose(); 
        }

    
    
    }

    /// <summary>
    /// DrawCommand: Draws a Point by a small cross
    /// </summary>
    public class DrawPoint : IDrawCommand
    {
        public DrawPoint(PointCoordinate p1, System.Drawing.Color color)
        {
            P1 = p1;
            Color = color;
        }

        public System.Drawing.Color Color;

        public bool Marked = false;
        public bool ExtraMarked = false;
        
        public PointCoordinate P1;

        private const int C_CrossWidth = 3;

        public void Draw(double offsetX, double offsetY, double zoom, System.Drawing.Graphics g)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color);
            int lineLength = C_CrossWidth;
            if (Marked == true)
            {
                lineLength+=3;
                pen.Width = 4;
            }

            System.Drawing.PointF P_1 = new System.Drawing.PointF(System.Convert.ToSingle(P1.X * zoom + offsetX - lineLength), System.Convert.ToSingle(P1.Y * zoom + offsetY));
            System.Drawing.PointF P_2 = new System.Drawing.PointF(System.Convert.ToSingle(P1.X * zoom + offsetX + lineLength ), System.Convert.ToSingle(P1.Y * zoom + offsetY));
            g.DrawLine(pen, P_1, P_2);

            System.Drawing.PointF P_3 = new System.Drawing.PointF(System.Convert.ToSingle(P1.X * zoom + offsetX), System.Convert.ToSingle(P1.Y * zoom + offsetY + lineLength));
            System.Drawing.PointF P_4 = new System.Drawing.PointF(System.Convert.ToSingle(P1.X * zoom + offsetX), System.Convert.ToSingle(P1.Y * zoom + offsetY -lineLength));
            g.DrawLine(pen, P_3, P_4);
            if (ExtraMarked == true)
                g.DrawArc(pen, System.Convert.ToSingle(P1.X * zoom + offsetX - lineLength), System.Convert.ToSingle(P1.Y * zoom + offsetY- lineLength), 2* lineLength, 2*lineLength, 0, 360) ;
 

            
            pen.Dispose();
        }
    
    }


    /// <summary>
    /// DrawCommand: Draws a Point by a small cross
    /// </summary>
    public class DrawCircle : IDrawCommand
    {
        public DrawCircle(PointCoordinate center, double radius, System.Drawing.Color color)
        {
            Center = center;
            Radius = radius;
            Color = color;
        }

        public System.Drawing.Color Color;
        public PointCoordinate Center;
        public double Radius;

        public void Draw(double offsetX, double offsetY, double zoom, System.Drawing.Graphics g)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color);
            Single rad = System.Convert.ToSingle(Radius * zoom);
            g.DrawArc(pen, System.Convert.ToSingle(Center.X * zoom + offsetX - rad), System.Convert.ToSingle(Center.Y * zoom + offsetY - rad), 2 * rad, 2 * rad, 0, 360);
            pen.Dispose();
        }

    }







}
