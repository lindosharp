﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormsTooling._2D.Drawing
{
    /// <summary>
    /// Control for drawing objects with the IDrawObject
    /// </summary>
    /// <remarks> Add an IDrawObject object to the Objects list. When calling DrawContainer.Refresh the appropriate draw command will be executed</remarks> 
    public class DrawContainer : System.Windows.Forms.Panel   
    {
        public DrawContainer()
        {
            EnableDoubleBuffering();
            Zoom = 1;
            Objects = new List<IDrawObject>(); 
        }

        public void EnableDoubleBuffering()
        {
            // Set the value of the double-buffering style bits to true.
            this.SetStyle(ControlStyles.DoubleBuffer |
               ControlStyles.UserPaint |
               ControlStyles.AllPaintingInWmPaint,
               true);
            this.UpdateStyles();
        }





        /// <summary>
        /// A zoom factor
        /// </summary>
        public double Zoom { get; set; }
        
        /// <summary>
        /// An offset in X direction
        /// </summary>
        public double X { get; set; }
        /// <summary>
        /// An offset in Y direction
        /// </summary>
        public double Y { get; set; }


        /// <summary>
        /// The List of IDrawObject objects. Each object has a IDrawCommand list.
        /// </summary>

        public List<IDrawObject> Objects;


        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            base.OnPaint(e);
            foreach (IDrawObject obj in Objects)
            {
                foreach (IDrawCommand cmd in obj.DrawCommands)
                {
                    cmd.Draw(X, Y, Zoom, e.Graphics); 
                }
            
            
            
            }



        }
    }
}
