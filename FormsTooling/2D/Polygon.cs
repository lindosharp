﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// A Polygon defined by Point Coordinates
    /// </summary>
    public class Polygon : PolygonBase 
    {
        public Polygon(PointCoordinate[] coordinates, bool drawInnerLines= false)
        {
            base.DrawInnerLines = drawInnerLines; 
            SetPoints(coordinates); 
        }
    }
}
