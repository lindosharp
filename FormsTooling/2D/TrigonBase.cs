﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// Basic container for trigonomy functions
    /// </summary>
    public abstract class TrigonBase
    {
        /// Returns the point of intersection from two lines (or null, if there is no intersection).  
        /// </summary>
        /// <param name="line1">first line</param>
        /// <param name="line2">second line</param>
        /// <returns>the point of intersection or null</returns>
        /// <remarks>if the two lines overlap, LinesIntersection starts looking at line1.P1
        /// </remarks> 
        protected static PointCoordinate LinesIntersection(Line line1, Line line2)
        {
            if (OverlappingLinePossible(line1, line2) == false)
                return null;

            double dx1 = line1.P2.X - line1.P1.X;
            double dx2 = line2.P2.X - line2.P1.X;

            //Both lines vertical
            if ((System.Math.Abs(dx1) <= C_DoubleCalcTolerance)  && (System.Math.Abs(dx2) <= C_DoubleCalcTolerance) )
            {
                return OverlappingLineIntersection(line1, line2);
            }


            double dy1, m1, b1, dy2, m2, b2, x, y;
            //only line1 vertical
            if ((System.Math.Abs(dx1) <= C_DoubleCalcTolerance))
            {
                // calculate linear formula for line2
                dy2 = line2.P2.Y - line2.P1.Y;
                m2 = dy2 / dx2;
                b2 = line2.P1.Y - m2 * line2.P1.X;

                // lookup for point of intersection
                x = line1.P1.X;
                y = m2 * x + b2;
                if (IsInRange(y, line1.P1.Y, line1.P2.Y))
                    return new PointCoordinate(x,y);
                else
                    return null;
            
            
            }

            //only line2 vertical
            if ((System.Math.Abs(dx2) <= C_DoubleCalcTolerance))
            {
                // calculate linear formula for line1
                dy1 = line1.P2.Y - line1.P1.Y;
                m1 = dy1 / dx1;
                b1 = line1.P1.Y - m1 * line1.P1.X;

                // lookup for point of intersection
                x = line2.P1.X;
                y = m1 * x + b1;
                if (IsInRange(y, line2.P1.Y, line2.P2.Y))
                    return new PointCoordinate(x, y);
                else
                    return null;
            }

            //no line vertical: lookup for point of intersection by substitute linear formula for both lines
            dy1 = line1.P2.Y - line1.P1.Y;
            m1 = dy1 / dx1;
            b1 = line1.P1.Y - m1 * line1.P1.X;
            dy2 = line2.P2.Y - line2.P1.Y;
            m2 = dy2 / dx2;
            b2 = line2.P1.Y - m2 * line2.P1.X;
            if (m2 == m1)
            {
                if (b1 != b2)
                    return null;
                else
                    return OverlappingLineIntersection(line1, line2);
            }
            else
            {
                x = (b1 - b2) / (m2 - m1);
                y = m1 * x + b1;
                if (IsInRange(x, line1.P1.X, line1.P2.X) &&
                    IsInRange(x, line2.P1.X, line2.P2.X) &&
                    IsInRange(y, line1.P1.Y, line1.P2.Y) &&
                    IsInRange(y, line2.P1.Y, line2.P2.Y))
                        return new PointCoordinate(x, y);
                else
                    return null;
            }

        }


        //rounding tolerance for double calculation
        private const double C_DoubleCalcTolerance = 0.5;

        /// <summary>
        /// Is val between Limit1 and Limit2?
        /// </summary>
        /// <param name="val"></param>
        /// <param name="Limit1"></param>
        /// <param name="Limit2"></param>
        /// <returns></returns>
        private static bool IsInRange(double val, double Limit1, double Limit2)
        {
            return (val >= System.Math.Min(Limit1, Limit2) - C_DoubleCalcTolerance) && (val <= System.Math.Max(Limit1, Limit2) + C_DoubleCalcTolerance);
        }
        
        /// <summary>
        /// Raw test: Is there an intersection or overlapping possible? Do the lines have possible ranges?
        /// </summary>
        /// <param name="line1">First line</param>
        /// <param name="line2">Second line</param>
        /// <returns>true: overlapping or intersection possible</returns>
        private static bool OverlappingLinePossible(Line line1, Line line2)
        {
            bool rval = true;
            //No overlapping range in X direction?
            if (System.Math.Max(line1.P1.Y, line1.P2.Y) < System.Math.Min(line2.P1.Y, line2.P2.Y) ||
                System.Math.Min(line1.P1.Y, line1.P2.Y) > System.Math.Max(line2.P1.Y, line2.P2.Y))
                rval = false;

            //No overlapping range in Y direction?
            if (System.Math.Max(line1.P1.X, line1.P2.X) < System.Math.Min(line2.P1.X, line2.P2.X) ||
                System.Math.Min(line1.P1.X, line1.P2.X) > System.Math.Max(line2.P1.X, line2.P2.X))
                rval = false;
            return rval;
        }


        
        /// <summary>
        /// Returns the Point of intersection of two (partitial or full) overlapping (m1= m2, b1=b2) lines 
        /// </summary>
        /// <param name="line1">first line</param>
        /// <param name="line2">second line</param>
        /// <returns>Point of overlappings started by line1.P1</returns>
        private static PointCoordinate OverlappingLineIntersection(Line line1, Line line2)
        {
            if (OverlappingLinePossible(line1, line2) == false)
                return null;
            
            //Line1.P1 is at line2 -> return Line1.P1
            if (line1.P1.Y > System.Math.Min(line2.P1.Y, line2.P2.Y) && line1.P1.Y <= System.Math.Max(line2.P1.Y, line2.P2.Y))
                    return line1.P1;

            //Depends on direction of line1 
            if (line1.P1.Y <= line1.P2.Y)
            {
                //Line1 starts at bottom
                return new PointCoordinate(System.Math.Min(line2.P1.X, line2.P2.X),   System.Math.Min(line2.P1.Y, line2.P2.Y));
            }
            else
            {
                //Line1 starts at top
                return new PointCoordinate(System.Math.Max(line2.P1.X, line2.P2.X),   System.Math.Max(line2.P1.Y, line2.P2.Y));
            }
        }


        
        /// <summary>
        /// Determines the angle between two lines
        /// </summary>
        /// <param name="Line1">First line</param>
        /// <param name="Line2">Second line</param>
        /// <returns>the angle</returns>
        protected static double AngleBetweenLines(Line Line1, Line Line2)
        {
            //Calculation by transformation: Get angle of first line, turn second line and return angle of turned second line  
            double dx, dy;
            double turnangle;
            turnangle = AngleOfLine(Line1);
            dx = Line2.P2.X - Line2.P1.X;
            dy = Line2.P2.Y - Line2.P1.Y;

            PolygonPoint P_2 = new PolygonPoint(dy * System.Math.Sin(turnangle) + dx * System.Math.Cos(turnangle),
                dy * System.Math.Cos(turnangle) - dx * System.Math.Sin(turnangle));
            Line l = new Line(new PointCoordinate(0,0), P_2 );
            return AngleOfLine(l);
        }


        /// <summary>
        /// Returns the angle of a line 
        /// </summary>
        /// <param name="line">the line</param>
        /// <returns>the angle</returns>
        protected static double AngleOfLine(Line line)
        {
            PointCoordinate  p1 = line.P1, p2 = line.P2;
            double rval = 0;
            Int32 iPre;
            double dx, dy;
            dx = p2.X - p1.X;
            dy = p2.Y - p1.Y;
            if (dy >= 0) //Quadrant determination
                iPre = (dx > 0 ? 0 : 1); 
            else
                iPre = (dx > 0 ? 3 : 2);

            if (dx != 0)
            {
                switch (iPre)
                {
                    case 0: //1st Quadrant
                        rval = System.Math.Atan(dy / dx);
                        break;
                    case 1: //2nd Quadrant
                        rval = System.Math.PI + System.Math.Atan(dy / dx);
                        break;
                    case 2: //3rd Quadrant
                        rval = System.Math.Atan(dy / dx) - System.Math.PI;
                        break;
                    case 3: //4th Quadrant
                        rval = System.Math.Atan(dy / dx);
                        break;
                }
            }

            else //Exception: vertical lines 
            {
                if (dy > 0)
                    rval = System.Math.PI / 2;
                else
                    rval = -System.Math.PI / 2;

            }
            return rval;
        }

        /// <summary>
        /// Determines distance between two points
        /// </summary>
        /// <param name="a">1st point</param>
        /// <param name="b">2nd point</param>
        /// <returns>length of line betwen points</returns>
        public static double DistanceBetweenPoints(PointCoordinate  a, PointCoordinate b)
        {
            return System.Math.Sqrt((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
        }

        /// <summary>
        /// Calculates the area of a triangle
        /// A= Sqr(s(s-a)(s-b)(s-c))) with s = (a+b+c)/2 (see Papula page 21) 
        /// </summary>
        /// <param name="a">1st Point</param>
        /// <param name="b">2nd Point</param>
        /// <param name="c">3rd Point</param>
        /// <returns>the calulated area</returns>
        protected double AreaOfTriangle(PolygonPoint a, PolygonPoint b, PolygonPoint c)
        {
            double rval = 0;
            double da, db, dc, s;
            da = DistanceBetweenPoints(b, c);
            db = DistanceBetweenPoints(a, c);
            dc = DistanceBetweenPoints(a, b);
            s = (da + db + dc) / 2;
            double p = s * (s - da) * (s - db) * (s - dc);
            if (p > 0)
                rval = System.Math.Sqrt(p);
            return rval;
        }

        protected bool PointAtLine(PointCoordinate pt, Line ln)
        {
            //Point in range of line?
            if (
                pt.X > System.Math.Max(ln.P1.X, ln.P2.X) ||
                pt.X < System.Math.Min(ln.P1.X, ln.P2.X) ||
                pt.Y < System.Math.Min(ln.P1.Y, ln.P2.Y) ||
                pt.Y > System.Math.Max(ln.P1.Y, ln.P2.Y))
                return false;


            double dx = ln.P2.X - ln.P1.X;
            double dy, m, b;

            //vertical line
            if ((System.Math.Abs(dx) <= C_DoubleCalcTolerance))
            {
                return true;
            }

            //non vertical line
            dy = ln.P2.Y - ln.P1.Y;
            m = dy / dx;
            b = ln.P1.Y - m * ln.P1.X;
            if (System.Math.Abs(pt.X * m + b - pt.Y) < C_DoubleCalcTolerance)
                return true;

            return false;
        }



    }
}
