﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// A Line with a start point, an angle and a maximum range
    /// </summary>
    public class Beam : Line 
    {
        public Beam(PointCoordinate p, double angl, double rg)
        {
            P1 = p;
            Angle = angl;
            Range = rg;
        }

        private double range;
        public double Range
        {
            get { return range; }
            set { range = value;  RecalculateP2(); }
        }


        private double angle;
        /// <summary>
        /// The angle to calculate the beams line in real world coordinates
        /// </summary>
        public double Angle
        {
            get { return angle; }
            set { angle = value; RecalculateP2(); }
        }


        protected override void OnP1Changed()
        {
            RecalculateP2();
        }

        private void RecalculateP2()
        {
            double dx = Range * System.Math.Cos(angle);
            double dy = Range * System.Math.Sin(angle);
            P2 = new PointCoordinate(P1.X + dx, P1.Y + dy); 
        }


        public PointCoordinate  PolygonIntersection(PolygonBase p)
        {
            //Optimize: Check if beam intersects outer rectangle of polygon
            PointCoordinate ptRet = null;
            int iptRet = 0xFFFF;
            for (int i = 0; i < p.points.Length; i++)
            { 
                Line ln;
                if (i == 0)
                    ln = new Line(p.points[0], p.points[p.points.Length - 1]);
                else
                    ln = new Line(p.points[i], p.points[i - 1]);
                
                PointCoordinate pt = LinesIntersection(this, ln);

                if (pt != null)
                {
                    if (ptRet == null)
                    {
                        ptRet = pt;
                        iptRet = i;
                    }
                    else
                    {
                        double oldDistance = DistanceBetweenPoints(P1, ptRet);
                        double newDistance = DistanceBetweenPoints(P1, pt);
                        if (newDistance < oldDistance)
                        {
                            ptRet = pt;
                            iptRet = i;
                        }

                    }
                }
            }
           
            return ptRet;
        }



        public PointCoordinate Test()
        {
            Line ln = new Line(new PointCoordinate(1000,1000), new PointCoordinate(0, 1000)  );
            return LinesIntersection(this, ln);
        }
    }
}
