﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// A circular polygon interpolated by lines
    /// </summary>
    public class Circle: PolygonBase 
    {
        public Circle(PointCoordinate center, double radius,  bool drawInnerLines= false)
        { 
            base.DrawInnerLines = drawInnerLines; 
            double alfaStep =  Drawing.RoundedRectHelper.MaxAngleFromRadius(System.Convert.ToInt32(System.Math.Ceiling(radius)));
            List<PointCoordinate> PointList = new List<PointCoordinate>();
            double alfa = 0;
            double alfaMax = System.Math.PI * 2;
            while (alfa < alfaMax)
            {
                double X = center.X + System.Math.Cos(alfa) * radius;
                double Y = center.Y + System.Math.Sin(alfa) * radius;
                PointList.Add (new PointCoordinate(X,Y));
                alfa += alfaStep;             
            }
            base.SetPoints(PointList.ToArray());  
        }
    }
}
