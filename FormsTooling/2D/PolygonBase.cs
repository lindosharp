﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace FormsTooling._2D
{
    /// <summary>
    /// The abstract polygon definition. Call SetPoints in constructor
    /// </summary>
    public abstract class PolygonBase : TrigonBase, FormsTooling._2D.Drawing.IDrawObject
    {

        #region types

        //Richtung des Polygons
        enum enmPolyDir              
        {
            dirLeft = 0,
            dirRight = 1
        }    
        #endregion

        private List<Drawing.IDrawCommand> drawCommands = new List<Drawing.IDrawCommand>();
        public List<Drawing.IDrawCommand> DrawCommands { get { return drawCommands; } private set { drawCommands = value;} } 
   
        #region VARIABLES
        private const double C_DoubleCalcTolerance = 0.5;

        private enmPolyDir PolygonDirection= enmPolyDir.dirRight; 
        public PolygonPoint[] points; //aktuell positionierte Ecken



        private Int32 m_pointCount;
        private Triangle[] m_Triangles=null; 
        #endregion



        /// <summary>
        /// Goes around from point to point and cuts outer triangles from polygon
        /// </summary>
        /// <returns>steps needed to cut down the polygon or 5000 if the polygon cannot be cutted down</returns>
        /// <remarks> a triangle can be cutted off, if:
        /// 1: it is an OUTER corner
        /// 2: No other point of polygon is in triangle area</remarks> 
        private Int32 CalculateTriangles()
        {
            PolygonDirection = GetPolygonDirection();
            Int32 tries=0;

            //Prepare triangle array
            m_Triangles = new Triangle[points.Length - 2];
            int TriangleIndex = 0;
            for (int i = 0; i < m_Triangles.Length; i++)        
            {
                Triangle triangle = new Triangle();
                triangle.TriangleAB = 0xFFFF;
                triangle.TriangleBC = 0xFFFF;
                triangle.TriangleAC = 0xFFFF;
                m_Triangles[i]= triangle; 
            }

            //Buffer for survived polygon points
            List<PolygonPoint> pts = new List<PolygonPoint>(points);



            int index1 = 0;
            
            while ((pts.Count >= 3) && (tries  < 5000))
            {
                index1 = (index1+1) % pts.Count;
                int index2 = (index1+1) % pts.Count;
                int index3 = (index2+1) % pts.Count;
                tries++;
                if (tries > 200)
                    System.Diagnostics.Debug.WriteLine("STOP");  
                //Test for outer edge
                double  angle = AngleBetweenLines(pts[index1], pts[index2], pts[index3]);
                if ((angle < 0 && PolygonDirection == enmPolyDir.dirRight) ||
                    (angle >= 0 && PolygonDirection == enmPolyDir.dirLeft))
                { 
                    //Test if another edge is in triangle
                    bool bMeets = false;
                    for (int i = 0; i < points.Length; i++)
                    {
                        if ((i != pts[index1].Index) && (i != pts[index2].Index) && (i != pts[index3].Index))
                        {
                            if (PointInTriangle(pts[index1].Index, pts[index2].Index, pts[index3].Index, points[i], true))
                            {
                                bMeets = true;
                                break;
                            }
                        }
                    }
                
                    if (bMeets == false) //Both tests good-> Triangle can be cut off
                    {
                        m_Triangles[TriangleIndex].a = pts[index1].Index;
                        m_Triangles[TriangleIndex].b = pts[index2].Index;
                        m_Triangles[TriangleIndex].c = pts[index3].Index;
                        GetTriangleNeighbours(TriangleIndex);
                        TriangleIndex++;
                        pts.RemoveAt(index2);
                    }
                }

            }

            System.Diagnostics.Debug.WriteLine(tries);
            return tries;
             
    
        }







        /// <summary>
        /// determines neighbour relation for a new trinagle in triangles array
        /// </summary>
        /// <param name="arrayIndex">The index of the new triangle from triangle array</param>
        ///<remarks>
        ///Useful to optimize repititive calls of PointInPolygon
        /// </remarks> 
        private void GetTriangleNeighbours(Int32 arrayIndex)
        {
            Int32 i;
            Int32 a , b , c , aComp , bComp , cComp;

            a = m_Triangles[arrayIndex].a;
            b = m_Triangles[arrayIndex].b;
            c = m_Triangles[arrayIndex].c;
    
            for (i = 0; i< arrayIndex ; i++)
            {
                aComp = m_Triangles[i].a;
                bComp = m_Triangles[i].b;
                cComp = m_Triangles[i].c;
                if (EqualPair(a, b, aComp, bComp)) 
                {    
                    m_Triangles[i].TriangleAB = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAB = i;
                }

                if (EqualPair(a, b, bComp, cComp)) 
                {
                    m_Triangles[i].TriangleBC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAB = i;
                }
            
            
                if (EqualPair(a, b, aComp, cComp))
                {
                    m_Triangles[i].TriangleAC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAB = i;
                }
        
                if (EqualPair(b, c, aComp, bComp))
                {
                    m_Triangles[i].TriangleAB = arrayIndex;
                    m_Triangles[arrayIndex].TriangleBC = i;
                }
        
                if (EqualPair(b, c, bComp, cComp))
                {
                    m_Triangles[i].TriangleBC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleBC = i;
                }
        
                if (EqualPair(b, c, aComp, cComp))
                {
                    m_Triangles[i].TriangleAC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleBC = i;
                }
    
                if (EqualPair(a, c, aComp, bComp))
                {
                    m_Triangles[i].TriangleAB = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAC = i;
                }

                if (EqualPair(a, c, bComp, cComp) )
                {
                    m_Triangles[i].TriangleBC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAC = i;
                }

                if (EqualPair(a, c, aComp, cComp))
                {
                    m_Triangles[i].TriangleAC = arrayIndex;
                    m_Triangles[arrayIndex].TriangleAC = i;
                }
            }
        }

        /// <summary>
        /// Are p1:p2 and p1Compare:p2Compare the same numbers?
        /// </summary>
        /// <param name="i1">an int</param>
        /// <param name="i2">an int</param>
        /// <param name="i1Compare">an int</param>
        /// <param name="i2Compare">an int</param>
        /// <returns>true, if (i1=i1compare and i2=i2compare) or (i1=i2compare and i2=i1compare )</returns>
        private bool EqualPair(Int32 i1 , Int32 i2 , Int32 i1Compare , Int32 i2Compare ) 
        {
            bool rval = false;
            if ((i1 == i1Compare) && (i2 == i2Compare )) 
                rval = true;
            else if ((i2 == i1Compare) && (i1 == i2Compare ))
                rval = true;
    
            return rval;

        }



        public bool PointInInnerPolygon(double x, double y, ref Int32 TriangleIndex)
        {
  
            if (PointInPolygon(x,y,ref TriangleIndex)  == false)
                return false;
            for (int i = 0; i < points.Length; i++)
            { 
                Line ln = new Line(points[i], points[(i+1)%(points.Length) ]); 
                if (PointAtLine(new PointCoordinate(x, y), ln))
                    return false;

            
            }

            return true;
        }



        //Liegt der angegebene Punkt im durch m_edges definierten Polygon
        //TriangleIndex: wenn angegeben, starte Suche im diesem Triangle,
        //               dann in den benachbarten Triangles, abschliessend in allen Triangles
        //               Gebe in TriangleIndex auch das gefundene Triangle zurück
        /// <summary>
        /// Is the x:y Point at this polygon?
        /// </summary>
        /// <param name="x">x coordinate of point</param>
        /// <param name="y">y coordinate of point</param>
        /// <param name="TriangleIndex">In: Starts searching at triangles[TriangleIndex]and its neighbours. Out: Returns the triangle index where the point is in</param>
        /// <returns>true, if the point stays on polygon (one of the triangles defining the polygon)</returns>
        /// <remarks>Optimize speed by using TriangleIndex: If you call repititive PointInPolygon with points standing nearby, cache returned TriangleIndex and use it in next call.
        /// </remarks> 
        public bool PointInPolygon(double x , double y, ref Int32  TriangleIndex) 
        {
            int i ;
            PolygonPoint p= new PolygonPoint(x, y);
            int Index=0;

            if (TriangleIndex != 0xFFFF)
            {
                for (i = 0; i <=3; i++)
                {
         
                    switch (i)
                    {
                        case 0: 
                            Index = TriangleIndex;
                            break;
                        case 1: 
                            Index = m_Triangles[TriangleIndex].TriangleAB;
                            break;
                        case 2: 
                            Index = m_Triangles[TriangleIndex].TriangleBC;
                            break;
                        case 3: 
                            Index = m_Triangles[TriangleIndex].TriangleAC;
                            break;
                    }   

                    if (Index != 0xFFFF)
                    {
                        if (PointInTriangle(m_Triangles[Index].a, m_Triangles[Index].b, m_Triangles[Index].c, p))
                            return true;
                    }
                }
            }
    
            for (i = 0;i< m_Triangles.Length;i++)
            {
                if (PointInTriangle(m_Triangles[i].a, m_Triangles[i].b, m_Triangles[i].c, p))
                {
                    if (TriangleIndex != 0xFFFF)
                        TriangleIndex = i;
                    return true;

                }
            }

            return false;
        }





        public bool DrawInnerLines = false;

        /// <summary>
        /// Called from constructor
        /// </summary>
        /// <param name="coordinates">Array of points determines the polygon corners</param>
        protected void SetPoints(PointCoordinate[] coordinates)
        {
            //Setup PolygonPoints array
            points = new PolygonPoint[coordinates.Length];
            m_pointCount = points.Length;
            for (int i = 0; i < m_pointCount;i++)
            {
                PolygonPoint ptEdge = new PolygonPoint(coordinates[i].X, coordinates[i].Y);
                ptEdge.Index = i;
                points[i] = ptEdge;
            }

            //Split into triangles
            CalculateTriangles();

            // Setup draw commands
            RecalcDraw();
        }


        public event EventHandler<EventArgs> CalculateTriangleFound; 

        /// <summary>
        /// Draw commands for forms applications
        /// </summary>
        private void RecalcDraw()
        {
            DrawCommands.Clear();  
            if (DrawInnerLines == true)
            {
                foreach (Triangle tri in m_Triangles)
                {
                    if (tri != null)
                    {
                        DrawCommands.Add(new Drawing.DrawLine(points[tri.a], points[tri.b], Color.Blue));
                        DrawCommands.Add(new Drawing.DrawLine(points[tri.b], points[tri.c], Color.Blue));
                        DrawCommands.Add(new Drawing.DrawLine(points[tri.a], points[tri.c], Color.Blue));
                    }
                }
            }
            for (int i = 0; i < points.Length; i++)
            {
                PointCoordinate pt1 = points[i];
                PointCoordinate pt2 = points[points.Length - 1];
                if (i != 0)
                {
                    pt2 = points[i - 1];
                }
                Drawing.DrawLine ln = new Drawing.DrawLine(pt1, pt2, Color.Black);
                DrawCommands.Add(ln);
            }

            if (CalculateTriangleFound != null)
                CalculateTriangleFound(this, new EventArgs());
        
        
        }


        //Bestimmt, ob der Punkt P innerhalb des über A, B und C definierten
        //Dreiecks liegt
        //Bestimmung läuft über Flächenvergleich:
        //Wenn die drei Dreiecke  (A,B,P), (A,C,P), (B,C,P)
        //die gleiche Fläche haben, wie das umschließende Dreieck (A,B,C),
        //dann liegt der Punkt innerhalb des Dreiecks
        //Inner = True: Liegt der Punkt auf einer der Ecken des
        //              Dreiecks, so wird dies explizit ausgenommen
        /// <summary>
        /// Determines, if point p stays at triangle with points(a,b,c)
        /// </summary>
        /// <param name="a">Index from points array describing 1st triangle corner</param>
        /// <param name="b">Index from points array describing 2nd triangle corner</param>
        /// <param name="c">Index from points array describing 3rd triangle corner</param>
        /// <param name="p"></param>
        /// <param name="Inner">true: only the inner area is inspected, edges of triangle are negated </param>
        /// <returns>true: Point is at triangle </returns>
        /// <remarks>A point stays at a triangle if the the three triangle (A,B,P), (A,C,P), (B,C,P) have the same area like triangle A,B,C </remarks>
        private bool PointInTriangle(Int32 a , Int32 b , Int32 c , PolygonPoint p , bool Inner = false) 
        {
            bool rval = false;
            double AreaCommon , Area1 , Area2 , Area3 ;
    
            AreaCommon = AreaOfTriangle(points[a], points[b], points[c]);
            if (AreaCommon == 0) return false;
            Area1 = AreaOfTriangle(points[a], points[b], p);
            Area2 = AreaOfTriangle(points[a], points[c], p);
            Area3 = AreaOfTriangle(points[b], points[c], p);
            if (System.Math.Abs(AreaCommon - (Area1 + Area2 + Area3)) < C_DoubleCalcTolerance) 
            {
                if (Inner)
                {
                    if (AreaCommon == 0)
                        return false;
                    if (
                        !(
                            PointAtLine(p, new Line( points[a], points[b])) ||
                            PointAtLine(p, new Line(points[a], points[c])) ||
                            PointAtLine(p, new Line(points[b], points[b]))
                        ))

                                rval= true;
                }

                else
                    rval= true;

            }
            return rval;    
        }




        /// <summary>
        /// Determines the direction of polygon definition (clock wise/ counter clock wise)
        /// </summary>
        /// <returns></returns>
        private enmPolyDir GetPolygonDirection() 
        {
            Int32  i;

            double angle=0;

            for (i = 0; i <points.Length - 2; i++)
            {
                angle = angle + AngleBetweenLines(points[i], points[i + 1], points[i + 2]);
            }
            angle = angle + AngleBetweenLines(points[i], points[i + 1], points[0]);
            angle = angle + AngleBetweenLines(points[i + 1], points[0], points[1]);
            if (angle > 0)
                return enmPolyDir.dirLeft ;
            else
                return enmPolyDir.dirRight;  

        }

        /// <summary>
        /// Determines the angle between two lines
        /// </summary>
        private double AngleBetweenLines(PolygonPoint p1, PolygonPoint p2, PolygonPoint p3) 
        {
            return AngleBetweenLines(new Line(p1, p2), new Line(p2, p3)); 
        }



        /// <summary>
        /// Returns the angle of a line 
        /// </summary>
        private double AngleOfLine(PolygonPoint p1, PolygonPoint p2) 
        {
            return AngleOfLine(new Line(p1, p2)); 
        }

        public Int32 edgecount() {return points.Length; }


        
        /// <summary>
        /// Maximum X coordinate of polygon
        /// </summary>
        public double MaxX
        {
            get
            {
                double rval = 0;
                foreach (PointCoordinate pt in points)
                {
                    if (pt.X > rval)
                        rval = pt.X;

                }
                return rval;
            }
        }


        /// <summary>
        /// Maximum Y coordinate of polygon
        /// </summary>
        public double MaxY
        {
            get
            {
                double rval = 0;
                foreach (PointCoordinate pt in points)
                {
                    if (pt.Y > rval)
                        rval = pt.Y;

                }
                return rval;
            }
        }


        /// <summary>
        /// Maximum X coordinate of polygon
        /// </summary>
        public double MinX
        {
            get
            {
                double rval = double.MaxValue;
                foreach (PointCoordinate pt in points)
                {
                    if (pt.X < rval)
                        rval = pt.X;

                }
                return rval;
            }
        }


        /// <summary>
        /// Maximum Y coordinate of polygon
        /// </summary>
        public double MinY
        {
            get
            {
                double rval = double.MaxValue;
                foreach (PointCoordinate pt in points)
                {
                    if (pt.Y < rval)
                        rval = pt.Y;

                }
                return rval;
            }
        }
        





    }
}
