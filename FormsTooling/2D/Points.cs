﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace FormsTooling._2D
{
    /// <summary>
    /// An extension for PointCoordinate used by PolygonBase
    /// </summary>
    public class PolygonPoint : PointCoordinate 
    {
        public PolygonPoint(double x, double y) : base(x, y) { }

        internal Int32 Index=0;
    }



    /// <summary>
    /// Simple Point declaration with x/y as double
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PointCoordinate
    {
        public PointCoordinate(double x, double y) 
        {
            X = x;
            Y = y;
        }
        public readonly double X; //x-Koordinate
        public readonly double Y; //y-Koordinate

        public bool Compare(PointCoordinate ptCompare)
        {
            return (this.X == ptCompare.X) && this.Y == (ptCompare.Y);
        }
    }

}
