﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// A line definition with intersection calculation
    /// </summary>
    public class Line : TrigonBase 
    {
        public Line() { } //Default constructor

        public Line(PointCoordinate p1, PointCoordinate p2)
        {
            P1 = p1;
            P2 = p2;
        }

        private PointCoordinate p1;
        public PointCoordinate P1
        {
            get { return p1; }
            set
            {
                p1 = value;
                OnP1Changed();        
            }
        
        }


        protected virtual void OnP1Changed ()
        {
            //Do nothing
        }

        public PointCoordinate P2;

        public double LineAngle
        {
            get { return AngleOfLine(this); } 
        }

        public PointCoordinate Intersection(Line line)
        {
            return LinesIntersection(this, line); 
        }



        //private void TestIntersection()
        //{
        //    Test(0, 0, 100, 100, 0, 100, 100, 0, "50,50"); //Crossing lines 
        //    Test(0, 0, 0, 100, 1, 100, 1, 0, "null"); //verical parallel lines
        //    Test(10, 0, 10, 100, 10, 20, 10, 200, "10,20"); //verical overlapping lines from bottom to top
        //    Test(10, 100, 10, 0, 10, 200, 10, 20, "10,100"); //verical overlapping lines from top to bottom
        //    Test(10, 100, 10, 0, 10, -2, 10, -20, "null"); //verical lines both at x= 10 with no intersection

        //    Test(10, 10, 110, 110, 30, 30, 150, 150, "30:30"); //lines with same m and b , but different start-/endpoints (l1<l2)
        //    Test(30, 30, 110, 110, 10, 10, 150, 150, "30:30"); //lines with same m and b , but different start-/endpoints (l1 in l2)
        //    Test(110, 110, 30, 30, 10, 10, 90, 90, "90:90"); //lines with same m and b , but different start-/endpoints (l1 > l2)
        //    Test(110, 110, 30, 30, 90, 90, 50, 50, "90:90"); //lines with same m and b , but different start-/endpoints (l2 in l1)

        //    Test(0, 0, 90, 00, 10, 0, 50, 0, "10:00"); //lines with m=0, 

        //}

        //void Test(double L1X1, double L1Y1, double L1X2, double L1Y2, double L2X1, double L2Y1, double L2X2, double L2Y2, string expected)
        //{
        //    Line lnA = new Line(new PointCoordinate(L1X1, L1Y1), new PointCoordinate(L1X2, L1Y2));
        //    Line lnB = new Line(new PointCoordinate(L2X1, L2Y1), new PointCoordinate(L2X2, L2Y2));

        //    TestResult(lnA.Intersection(lnB), expected);
        //}

        //private void TestResult(PointCoordinate pt, string expected)
        //{
        //    string s = "No intersection";
        //    if (pt != null)
        //        s = "X = " + pt.X.ToString("0.00") + " Y = " + pt.Y.ToString("0.00");
        //    System.Diagnostics.Debug.WriteLine(s + " Expected:  " + expected);
        //}




        
    }
}
