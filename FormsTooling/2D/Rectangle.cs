﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    public class Rect : PolygonBase
    {
        /// <summary>
        /// A Rectangle with polygon functions
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="drawInnerLines"></param>
        public Rect(System.Drawing.RectangleF rect, bool drawInnerLines= false)
        {
            base.DrawInnerLines = drawInnerLines; 
            base.SetPoints(new PointCoordinate[] { new PointCoordinate(rect.X, rect.Y), new PointCoordinate(rect.X + rect.Width, rect.Y), new PointCoordinate(rect.X + rect.Width, rect.Y + rect.Height), new PointCoordinate(rect.X, rect.Y + rect.Height)});
        }
    }
}
