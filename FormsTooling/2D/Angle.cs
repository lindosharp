﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    public class Angle
    {

        public Angle(double rad)
        {
            Radian = rad;
        }

        private double radian = 0;
        public double Radian
        {
            get {return radian;}
            set
            {
                radian = value;

                //Normalize to 0..2PI
                while (radian < 0)
                    radian += Math.PI * 2;

                while (radian >= 2* Math.PI)
                    radian -= Math.PI * 2;
            }
        }

        public double Degree
        {
            get { return ToDegree(radian); }
            set { Radian = ToRad(value); }
        }

        static double ToRad(double degree)
        {
            return (degree * Math.PI) / 180;
        }

        static double ToDegree(double rad)
        {
            return rad * 180 / Math.PI;        
        }
    }
}
