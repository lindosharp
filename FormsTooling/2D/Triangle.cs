﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling._2D
{
    /// <summary>
    /// Used by PolygonBase
    /// </summary>
    internal class Triangle
    {
        internal Int32 a ; //Verweis auf 1. Punkt
        internal Int32 b; //Verweis auf 2. Punkt
        internal Int32 c; //Verweis auf 3. Punkt
        //internal double area; // Fläche des Dreiecks
        internal Int32 TriangleAB; //Index des benachbarten Dreiecks (an Linie AB angrenzend)
        internal Int32 TriangleBC;//Index des benachbarten Dreiecks (an Linie BC angrenzend)
        internal Int32 TriangleAC; //Index des benachbarten Dreiecks (an Linie AC angrenzend)
    }
}
