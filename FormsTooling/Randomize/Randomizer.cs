﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling.Randomize
{
    public class Randomizer
    {

        static System.Random random = null;

        static Randomizer()
        {
            Init();
        }
        static void Init()
        {
            if (random == null)
                random = new System.Random((int)System.DateTime.Now.Ticks % int.MaxValue);
        }

        public static int Random(int min, int max)
        {
            if (min == max)
                return min;

            return random.Next(min, max+1);               
        }

        public static int Random(int max)
        {

            return Random(0, max);
        }


        public static double Random(double min, double max)
        {
            if (min == max)
                return min;

            double rval =  random.NextDouble();
            rval = rval * (max - min) + min;
            return rval;
        }

        public static double Random(double max)
        {
            return Random(0 , max);
        }

        public static bool Random()
        {
            return Random(1) == 1;
        }
    }
}
