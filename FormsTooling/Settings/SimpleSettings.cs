﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsTransfer
{
    //Bypass to the settings class without WindowsForms reference
    public class SimpleSettings
    {
        Settings settings = null; 
        public SimpleSettings(object source)
        {
            settings = new Settings(source);
        }


        public string Path 
        { 
            get {return settings.Path; }
            set { settings.Path = value; }
        }
        
        public string FileName 
        {
            get { return settings.FileName; }
            set { settings.FileName = value; }
        }

        
        public void LoadImmediately()
        {
            settings.LoadImmediately();     
        }

    }
}
