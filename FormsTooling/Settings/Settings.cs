﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using SettingsTransfer.Controls;
 
namespace SettingsTransfer
{
    /// <summary>
    /// Interacts with ISettingsControl-Controls. Data transfer: Controls <--> Object properties<--> XML file
    /// </summary>
    /// <remarks> If you have an object with properties which are displayed in controls that are implementing ISettingsControl, 
    /// use Settings.Read/Write to transfer data between Controls and object.
    /// Use Load/Save/Path/FileName to transfer property data to/from file.
    /// </remarks>
    public class Settings
    {
        object source;
        System.Windows.Forms.Control target;
        public Settings(object src, System.Windows.Forms.Control tgt= null)
        {
            source = src;
            target = tgt;

            Path = Application.StartupPath;
            FileName = "ControlSettings.xml";
            if (tgt != null)
                Find_ISettingsControls(target);

        
        }


        public string Path { get; set; }
        public string FileName { get; set; }

        private String GetFilePath()
        { 
            return Path + "\\" + FileName; 
        }

        #region Read/Write


        
        /// <summary>
        /// Reads settings values from source to controls
        /// </summary>
        public void Read()
        {
            foreach (ISettingsControl cnt in FoundControls)
            {
                string[] SourceProperties = cnt.SourcePropertyName.Split(';');
                string SourceProperty = SourceProperties[0];
                //TransferPropertyValue(source, cnt, SourceProperty, cnt.ValuePropertyName);
                TransferPropertyValueExt(cnt, SourceProperty, cnt.ValuePropertyName, SettingsTransferDirection.ToControl);
            }
        }



        /// <summary>
        /// writes settings values from controls to source
        /// </summary>
        public void Write()
        {
            foreach (ISettingsControl cnt in FoundControls)
            {
                string[] SourceProperties = cnt.SourcePropertyName.Split(';');

                foreach (string s in SourceProperties)
                {
                    string SourceProperty  = s.Trim();
                    //TransferPropertyValue(cnt, source, cnt.ValuePropertyName, SourceProperty);
                    TransferPropertyValueExt(cnt, SourceProperty, cnt.ValuePropertyName, SettingsTransferDirection.ToSource );
                }
            }
        }

        
        

        private PropertyInfo FindProperty(object o, string propertyName)
        { 
            PropertyInfo rval = null;
            System.Reflection.MemberInfo[] mInfos = o.GetType().GetMember(propertyName);

            if (mInfos.Length > 0)
            {
                System.Reflection.MemberInfo mi = mInfos[0];
                System.Reflection.PropertyInfo pi = (System.Reflection.PropertyInfo)mi;
                rval = pi;
            }
            return rval;
        }

        private object GetPropertyValue(object o, string propertyName)
        {
            object rval = null;
            if (FindProperty(o, propertyName) != null) 
                rval = o.GetType().InvokeMember(propertyName, System.Reflection.BindingFlags.GetProperty | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance, null, o, null);
            return rval;
        }


        private bool SetPropertyValue(object o, string propertyName, object val)
        {
            bool rval = false;
            if (FindProperty(o, propertyName) != null)
            {
                o.GetType().InvokeMember(propertyName, System.Reflection.BindingFlags.SetProperty | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance, null, o, new object[] { val });
                rval = true;
            }
            return rval;
        }

        public void TransferPropertyValue(object oSource, object oTarget, string sourcePropertyName, string targetPropertyName)
        {
            //Find source property
            PropertyInfo piSource = FindProperty(oSource, sourcePropertyName);
            if (piSource != null)
            {
                PropertyInfo piTarget = FindProperty(oTarget, targetPropertyName);
                if (piTarget != null)
                {
                    object SourceValue = GetPropertyValue(oSource, sourcePropertyName);
                    if (piSource.PropertyType.GetInterface("IConvertible") != null)
                    {
                        IConvertible sourceConverter = (IConvertible)SourceValue;
                        object TargetValue = sourceConverter.ToType(piTarget.PropertyType, Application.CurrentCulture);

                        SetPropertyValue(oTarget, targetPropertyName, TargetValue);
                    }
                }
            }
        }



        public void TransferPropertyValueExt(ISettingsControl cnt, string sourcePropertyName, string controlPropertyName, SettingsTransferDirection dir)
        {
            //Find source and control property
            PropertyInfo piSource = FindProperty(source, sourcePropertyName);
            if (piSource == null)
                return;

            PropertyInfo piControl = FindProperty(cnt, controlPropertyName);
            if (piControl == null)
                return;

            SettingsTransferEventArgs settingsTransfer = new SettingsTransferEventArgs();
            settingsTransfer.Direction = dir;
            settingsTransfer.ControlValue  = GetPropertyValue(cnt, controlPropertyName);
            settingsTransfer.OriginalSourceValue = GetPropertyValue(source, sourcePropertyName);


            if (dir == SettingsTransferDirection.ToSource)
            {  //Control -> Source
                IConvertible Converter = (IConvertible)settingsTransfer.ControlValue;
                settingsTransfer.ConvertedSourceValue = Converter.ToType(piSource.PropertyType, Application.CurrentCulture);
                //Send an event request to control for value conversion
                cnt.RaiseSettingsTransfer (settingsTransfer);
                SetPropertyValue(source, sourcePropertyName, settingsTransfer.ConvertedSourceValue);
            }
            else
            { //Source-> Control 
                IConvertible sourceConverter = (IConvertible)settingsTransfer.OriginalSourceValue;
                settingsTransfer.ConvertedSourceValue = sourceConverter.ToType(piControl.PropertyType, Application.CurrentCulture);
                //Send an event request to control for value conversion
                cnt.RaiseSettingsTransfer (settingsTransfer);
                SetPropertyValue(cnt, controlPropertyName, settingsTransfer.ConvertedSourceValue);
            }

        }






        #endregion





        #region Load/Save

        public void Save()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml("<?xml version='1.0' ?>" +
                "<Settings>" +
                "</Settings>");

    

            foreach (ISettingsControl cnt in FoundControls)
            {
                string[] sourceProperties = cnt.SourcePropertyName.Split(';');
                foreach (string sprop in sourceProperties)
                {
                    string sourceProperty = sprop.Trim();
                    PropertyInfo piSource = FindProperty(source, sourceProperty);
                    if (piSource != null)
                    {

                        System.Xml.XmlAttribute attr = doc.DocumentElement.Attributes[sourceProperty];
                        if (attr== null)
                        {
                            object SourceValue = GetPropertyValue(source, sourceProperty);
                            string attrVal = SourceValue.ToString();
                            attr= doc.CreateAttribute(sourceProperty);
                            attr.Value = attrVal;
                            doc.DocumentElement.Attributes.Append(attr);
                        }
                    }
                }
            }
            String s = doc.OuterXml;

            doc.Save(GetFilePath());
        
        }



        /// <summary>
        /// Loads property values that are bound with ISettingControl at the actual form from xml file to the object
        /// </summary>
        public void Load()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(GetFilePath());

            foreach (ISettingsControl cnt in FoundControls)
            {
                string[] sourceProperties = cnt.SourcePropertyName.Split(';');
                foreach (string sprop in sourceProperties)
                {
                    string sourceProperty = sprop.Trim();

                    PropertyInfo piSource = FindProperty(source, sourceProperty );
                    if (piSource != null)
                    {
                        System.Xml.XmlAttribute xmlAttr = doc.DocumentElement.Attributes[sourceProperty];
                        if (xmlAttr != null)
                        {
                            string value = xmlAttr.Value;
                            IConvertible converter = (IConvertible)value;
                            object oValue = converter.ToType(piSource.PropertyType, Application.CurrentCulture);
                            SetPropertyValue(source, sourceProperty, oValue);
                        }
                    }
                }

            }
        }

        #endregion

        #region Load without control binding

        /// <summary>
        /// Loads all property values from xml file to the object
        /// </summary>
        public void LoadImmediately()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load(GetFilePath());

            foreach (System.Xml.XmlAttribute attr  in doc.DocumentElement.Attributes )
            {
                string sourceProperty = attr.Name;

                PropertyInfo piSource = FindProperty(source, sourceProperty );
                if (piSource != null)
                {
                        string value = attr.Value;
                        IConvertible converter = (IConvertible)value;
                        object oValue = converter.ToType(piSource.PropertyType, Application.CurrentCulture);
                        SetPropertyValue(source, sourceProperty, oValue);
                }
            }
        }




        #endregion



        #region Find controls with ISettingControl
        List <ISettingsControl> FoundControls   = new List<ISettingsControl>();
 
        private void Find_ISettingsControls(Control parent)
        { 
            FoundControls.Clear();
            RecFind_ISettingsControls(parent);
        }

        private void RecFind_ISettingsControls(Control parent)
        {
            foreach (Control cnt in parent.Controls)
            { 
                Type tp = cnt.GetType();
                if (tp.GetInterface(typeof (ISettingsControl).Name) != null)
                {
                    ISettingsControl Icnt = (ISettingsControl) cnt;
                    FoundControls.Add(Icnt);                     
                }

                RecFind_ISettingsControls(cnt); 
            }
        }
        #endregion
    }
}
