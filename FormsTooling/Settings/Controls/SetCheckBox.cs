﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsTransfer.Controls
{
    public class SetCheckBox : System.Windows.Forms.CheckBox , ISettingsControl  
    {
        public string ValuePropertyName
        {
            get { return "Checked"; }
        }

        public string SourcePropertyName{ get; set;}

        public event EventHandler<SettingsTransferEventArgs> SettingsTransfer;

        public void RaiseSettingsTransfer(SettingsTransferEventArgs e)
        {
            if (SettingsTransfer != null)
                SettingsTransfer(this, e);
        }
    }
}
