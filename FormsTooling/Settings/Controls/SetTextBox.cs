﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsTransfer.Controls
{
    public class SetTextBox : System.Windows.Forms.TextBox, ISettingsControl 
    {
        public string ValuePropertyName
        {
            get { return "Text"; }
        }

        public event EventHandler<SettingsTransferEventArgs> SettingsTransfer;
        public string SourcePropertyName{get; set;}

        public void RaiseSettingsTransfer(SettingsTransferEventArgs e)
        {
            if (SettingsTransfer  != null)
                SettingsTransfer(this, e);

        }



    
    
    }
}
