﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SettingsTransfer.Controls
{
    public interface  ISettingsControl
    {
        string ValuePropertyName { get; }
        string SourcePropertyName { get; set; }
        event EventHandler<SettingsTransferEventArgs> SettingsTransfer;
        void RaiseSettingsTransfer(SettingsTransferEventArgs e);
    }

    public enum SettingsTransferDirection
    { 
        ToSource = 1,
        ToControl = 2
    }

    public class SettingsTransferEventArgs : EventArgs
    { 
        public SettingsTransferDirection Direction {get; internal set;}
        public object ControlValue { get; set; }
        public object ConvertedSourceValue { get; set; }
        public object OriginalSourceValue { get; set; }
    }

}
