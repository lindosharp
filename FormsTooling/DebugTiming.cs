﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling
{
    public class DebugTiming
    {
        static DateTime lastTime;
        public static string TimeMeasure(string s, bool reset = false)
        {
            string sret = "TimeMeasure: " + s + " ";

            if (reset)

                lastTime = System.DateTime.Now;
            else
            {
                string sTime = DateTime.Now.Subtract(lastTime).TotalMilliseconds.ToString() + "[ms]";
                sret += sTime;
            }
            System.Diagnostics.Debug.WriteLine(sret);
            return sret;
        }

    }
}
