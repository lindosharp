﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormsTooling.Compare
{
    public class CompareBase
    {
        public delegate bool CompareFunc(int Index1, int Index2);
        protected int[,] d;



        protected int Count1 = 0;
        protected int Count2 = 0;

        protected void PrepareField(int count1, int count2)
        {
            if (Count1 != count1 || Count2 != count2)
                d = new int[count1 + 1, count2 + 1];
            //else
            //    for (int i = 0; i < count1 + 1; i++)
            //        for (int j = 0; j < count2 + 1; j++)
            //            d[i, j] = 0;

            Count1 = count1;
            Count2 = count2;
        }

        //public double DamerauLevenshteinDistance(CompareFunc func)
        //{
        //    int i, j, cost;

        //    for (i = 1; i <= Count1; i++)
        //    {
        //        for (j = 1; j <= Count2; j++)
        //        {
        //            if (func(i-1, j-1) == true )
        //                cost = 0;
        //            else
        //                cost = 1;


        //            d[i, j] = Math.Min(d[i - 1, j] + 1, Math.Min(d[i, j - 1] + 1, d[i - 1, j - 1] + cost));

        //            if ((i > 1) && (j > 1))
        //            {
        //                if (func(i - 1, j - 2) == true && func (i - 2, j - 1)== true)
        //                {
        //                    d[i, j] = Math.Min(d[i, j], d[i - 2, j - 2] + cost);
        //                }
        //            }
        //        }
        //    }
        //    double Distance = d[d.GetLength(0)-1 , d.GetLength(1)-1];
        //    double rval = (Distance / System.Math.Min(d.GetLength(0), d.GetLength(1)));
        //    return rval;

        //}

        public double DamerauLevenshteinDistance(CompareFunc func)
        {
            int i, j, cost;

            for (i = 1; i <= Count1; i++)
            {
                for (j = 1; j <= Count2; j++)
                {
                    if (func(i - 1, j - 1) == true)
                        cost = 0;
                    else
                        cost = 1;


                    int min1 = d[i - 1, j] + 1;
                    int min2 = d[i, j - 1] + 1;
                    int min3 = d[i - 1, j - 1] + cost;

                    if (min2 < min1)
                        min1 = min2;
                    if (min3 < min1)
                        min1 = min3;
                    d[i, j] = min1;
                }
            }
            double Distance = d[d.GetLength(0) - 1, d.GetLength(1) - 1];
            double rval = (Distance / System.Math.Min(d.GetLength(0), d.GetLength(1)));
            return rval;

        }











    }
}
