﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    public class Route
    {
        RouteSettings settings = null;

        int actual = 0;
        RouteBufferEntry[] buffer;
 
        public Route(RouteSettings routeSettings)
        {
            settings = routeSettings;
            //Init ring buffer
            buffer = new RouteBufferEntry[settings.BufferDepth];
            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = new RouteBufferEntry();
        }


        public void AddMatches(List<MapRasterPoint> matches, World map)
        {


            //Reset extramarked in Map raster
            foreach (MapRasterPoint pt in map.Raster)
                pt.DrawObject.ExtraMarked = false;
  
            int previous = actual;
            actual= (actual + 1)% settings.BufferDepth;
            RouteBufferEntry actualEntry = buffer[actual];
            RouteBufferEntry previousEntry = buffer[previous];
            
            actualEntry.Points.Clear();
            foreach (MapRasterPoint pt in matches)
            {
                
                RouteBufferPoint bufferPoint = new RouteBufferPoint(pt);
                //Get neighbours and compare with previous found points
                List<MapRasterPoint> neighbours = map.FindNeighbours(pt.X, pt.Y, settings.NeighbourHoodDistance );
                foreach (MapRasterPoint neighbour in neighbours)
                {
                    foreach (RouteBufferPoint previousPoint in previousEntry.Points)
                    {
                        if ((previousPoint.CompareCoordinates(neighbour.X, neighbour.Y) == true) || previousPoint.CompareCoordinates(pt.X, pt.Y))
                        { 
                            // found a match
                            bufferPoint.From.Add(previousPoint);
                            if (bufferPoint.depth < previousPoint.depth + 1)
                                bufferPoint.depth = previousPoint.depth + 1;

                            if (bufferPoint.depth > settings.MinDepth)
                                bufferPoint.RasterPoint.DrawObject.ExtraMarked = true;  
                        }
                    }
                }
                
                actualEntry.Points.Add(bufferPoint);
            
            }
        
        
        }

    }


    public class RouteBufferPoint
    {
        public RouteBufferPoint(MapRasterPoint rasterPoint)
        {
            RasterPoint = rasterPoint;
        }

        public MapRasterPoint RasterPoint = null;
        public List<RouteBufferPoint> From = new List<RouteBufferPoint> ();
        public int depth;
        public bool CompareCoordinates(double x, double y)
        {
            return (x == RasterPoint.X) && (y == RasterPoint.Y);
        }
    
    }


    public class RouteBufferEntry
    {
        public List<RouteBufferPoint> Points = new List<RouteBufferPoint> ();
    }

    public class RouteSettings
    {
        public int BufferDepth = 20;
        public double NeighbourHoodDistance = 150;
        public int MinDepth = 5;
    }
}
