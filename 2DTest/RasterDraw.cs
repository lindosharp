﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    class DrawObject : FormsTooling._2D.Drawing.IDrawObject    
    {
        private List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands = new List<FormsTooling._2D.Drawing.IDrawCommand>();
        public List<FormsTooling._2D.Drawing.IDrawCommand> DrawCommands { get { return drawCommands; } private set { drawCommands = value; } } 


    }
}
