﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    public class MapRasterPoint
    {
        public MapRasterPoint(double x, double y)
        {
            X = x;
            Y = y;
            DrawObject = new FormsTooling._2D.Drawing.DrawPoint(new FormsTooling._2D.PointCoordinate(x,y), System.Drawing.Color.Black);  
        }

        public double X;
        public double Y;
        public DistanceHistogramm Distances;
        // public BarkHistogramm Barks

        public FormsTooling._2D.Drawing.DrawPoint DrawObject { get; private set; }
    }
}
