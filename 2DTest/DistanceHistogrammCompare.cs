﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{

    public enum DistanceHistogrammCompareMode
    { 
        Distances = 0,
        Significants = 1
    }

    class DistanceHistogrammCompare : FormsTooling.Compare.CompareBase 
    {
        public DistanceHistogrammCompare(double absoluteDistanceMatch, DistanceHistogrammCompareMode mode)
        {
            AbsoluteDistanceMatch = absoluteDistanceMatch;
            Mode = mode;
        }

        private DistanceHistogramm histogramm1, histogramm2;
        public DistanceHistogrammCompareMode Mode;
        
        
        public double Compare(DistanceHistogramm hist1, DistanceHistogramm hist2)
        {
            histogramm1 = hist1;
            histogramm2 = hist2;
            if (Mode == DistanceHistogrammCompareMode.Distances)
            {
                PrepareField(hist1.Beams.Count, hist2.Beams.Count);
                return DamerauLevenshteinDistance(MatchDistance);
            }
            else
            {
                PrepareField(hist1.Significant.Count, hist2.Significant.Count);
                return DamerauLevenshteinDistance(MatchSignificant);
            }
        }


        public double AbsoluteDistanceMatch = 100;
        private bool MatchDistance(int i, int j)
        {
            return (System.Math.Abs(histogramm1.Beams[i].Distance - histogramm2.Beams[j].Distance) < AbsoluteDistanceMatch);
        }

        private bool MatchSignificant(int i, int j)
        {
            SignificantSign sign1 = histogramm1.Significant[i];
            SignificantSign sign2 = histogramm2.Significant[j];
            bool bret = false;

            if (sign1.Type == sign2.Type)
            {
                //if (System.Math.Abs(sign1.Width - sign2.Width) < 25)
                //    if (System.Math.Abs(sign1.Value - sign2.Value) < 100)
                //        bret = true;
                if (RelativeAbsoluteDeviation(sign1.Width, sign2.Width, 10, 0.35))
                    if (RelativeAbsoluteDeviation(sign1.Value, sign2.Value, 200, 0.35))
                        bret = true;

            }
            return bret;

        }

        private bool RelativeAbsoluteDeviation(double x, double y, double minAbsolute, double maxRelative)
        {
            double diff = System.Math.Abs(x - y);
            if (System.Math.Max(x, y) <= minAbsolute)
                return true;
            if (diff / System.Math.Max(x, y) < maxRelative)
                return true;
            return false;

        
        
        }

    
    
    
    
    }
}
