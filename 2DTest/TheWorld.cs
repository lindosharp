﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;

namespace _2DTest
{
    class TheWorld : World
    {

        public TheWorld(WorldSettings worldsetngs) : base (worldsetngs) 
        {
            FormsTooling._2D.PointCoordinate[] outerCoordinates = new FormsTooling._2D.PointCoordinate[]
            {
                new FormsTooling._2D.PointCoordinate(0,0),
                new FormsTooling._2D.PointCoordinate(1000,0),
                new FormsTooling._2D.PointCoordinate(1000,400),
                new FormsTooling._2D.PointCoordinate(1200,400),
                new FormsTooling._2D.PointCoordinate(1200,0),
                new FormsTooling._2D.PointCoordinate(2200,0),
                new FormsTooling._2D.PointCoordinate(2400,200),
                new FormsTooling._2D.PointCoordinate(2600,0),
                new FormsTooling._2D.PointCoordinate(3600,0),
                new FormsTooling._2D.PointCoordinate(3600,100),
                new FormsTooling._2D.PointCoordinate(3500,100),
                new FormsTooling._2D.PointCoordinate(3500,200),
                new FormsTooling._2D.PointCoordinate(3600,200),
                new FormsTooling._2D.PointCoordinate(3600,300),
                new FormsTooling._2D.PointCoordinate(3500,300),
                new FormsTooling._2D.PointCoordinate(3500,400),
                new FormsTooling._2D.PointCoordinate(3600,400),

                new FormsTooling._2D.PointCoordinate(3600,1000),
                new FormsTooling._2D.PointCoordinate(2600,1000),
                new FormsTooling._2D.PointCoordinate(2600,400),
                new FormsTooling._2D.PointCoordinate(2400,600),
                new FormsTooling._2D.PointCoordinate(2200,400),
                new FormsTooling._2D.PointCoordinate(2200,1000),
                new FormsTooling._2D.PointCoordinate(1800,1000),
                new FormsTooling._2D.PointCoordinate(1800,1300),
                new FormsTooling._2D.PointCoordinate(2200,1300),
                new FormsTooling._2D.PointCoordinate(2200,2300),
                new FormsTooling._2D.PointCoordinate(2000,2300),
                new FormsTooling._2D.PointCoordinate(2000,2000),
                new FormsTooling._2D.PointCoordinate(1800,2000),
                new FormsTooling._2D.PointCoordinate(1800,2300),
                new FormsTooling._2D.PointCoordinate(1500,2300),
                new FormsTooling._2D.PointCoordinate(1500,2000),
                new FormsTooling._2D.PointCoordinate(1300,2000),
                new FormsTooling._2D.PointCoordinate(1300,1300),
                new FormsTooling._2D.PointCoordinate(1600,1300),
                new FormsTooling._2D.PointCoordinate(1600,1000),
                new FormsTooling._2D.PointCoordinate(1200,1000),
                new FormsTooling._2D.PointCoordinate(1200,600),
                new FormsTooling._2D.PointCoordinate(1000,600),
                new FormsTooling._2D.PointCoordinate(1000,1000),
                new FormsTooling._2D.PointCoordinate(0,1000)
            };


            OuterPolygon = new FormsTooling._2D.Polygon(outerCoordinates);
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(500, 500), 200));
            InnerPolygons.Add(new FormsTooling._2D.Rect(new System.Drawing.RectangleF(1500, 200, 200, 500)));
            InnerPolygons.Add(new FormsTooling._2D.Rect(new System.Drawing.RectangleF(3400, 900, 200, 100)));

            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(2700, 60), 50));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(2850, 60), 30));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3000, 60), 70));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3150, 60), 20));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3300, 60), 90));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 60), 40));

            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 180), 50));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 240), 30));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 366), 70));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 466), 20));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 611), 90));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 755), 40));

            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(2700, 800), 50));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(2850, 600), 30));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3000, 750), 70));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3150, 900), 20));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3300, 876), 90));
            InnerPolygons.Add(new FormsTooling._2D.Circle(new PointCoordinate(3450, 960), 40));



        
        
        }
    }
}
