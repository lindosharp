﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;

namespace _2DTest
{
    public class DistanceHistogramm : TrigonBase 
    {
        public List<DistanceHistogrammBeam> Beams = new List<DistanceHistogrammBeam>();
        public double Range;
        public int AngleResolution;

        public List<SignificantSign> Significant = new List<SignificantSign>();

        public DistanceHistogramm(int anglResolution, double rng)
        {
            this.Range = rng;
            this.AngleResolution = anglResolution; 
        }



        public void CalculateSignificant()
        { 
            SignificantSign sign = new SignificantSign(0); 
            
            foreach( DistanceHistogrammBeam beam in Beams)
            {
                if (sign.Add (beam.Distance, beam.Angle ) == false)
                {
                    Significant.Add(sign);
                    sign = new SignificantSign(beam.Angle );
                }
            }
            if (sign.Width > 0 )
                Significant.Add(sign);

            
            string sSignificant = Significant.Count.ToString();
        }

        /// <summary>
        /// Transforms to relative sight (P1 = (0,0), angle=0)
        /// </summary>
        /// <returns></returns>
        public DistanceHistogramm Normalize()
        {
            DistanceHistogramm rval = new DistanceHistogramm(AngleResolution, Range); 
            for (int i = 0 ; i < Beams.Count  ; i++)
            { 
                DistanceHistogrammBeam beam = Beams[i];
                PointCoordinate pt1 = new PointCoordinate(0, 0);

                PointCoordinate pt2 = null;
                PointCoordinate pt2Error = null;

                if (beam.P2 != null)
                {
                    pt2 = new PointCoordinate(beam.P2.X - beam.P1.X, beam.P2.Y - beam.P1.Y);
                    pt2Error = new PointCoordinate(beam.P2Error.X - beam.P1.X, beam.P2Error.Y - beam.P1.Y);
                }
                double angle = (i* AngleResolution * Math.PI) / 180;

                DistanceHistogrammBeam newBeam = new DistanceHistogrammBeam(pt1, pt2, beam.Distance, angle, i, rval);
                newBeam.P2Error = pt2Error;
                rval.Beams.Add (newBeam);
            }
            return rval;
        
        }


        public DistanceHistogramm Transform(PointCoordinate offset, double angle)
        {
            DistanceHistogramm rval = new DistanceHistogramm(AngleResolution, Range);
            for (int i = 0; i < Beams.Count; i++)
            {
                DistanceHistogrammBeam beamOrg = Beams[i];
                PointCoordinate pt1 = new PointCoordinate(beamOrg.P1.X + offset.X,  beamOrg.P1.Y + offset.Y);

                PointCoordinate pt2 = null;
                PointCoordinate pt2Error = null;
                if (beamOrg.P2 != null)
                {
                    pt2 = new PointCoordinate(beamOrg.P2.X + offset.X, beamOrg.P2.Y + offset.Y);
                    pt2Error = new PointCoordinate(beamOrg.P2Error.X + offset.X, beamOrg.P2Error.Y + offset.Y);
                }
                double angleNew = beamOrg.Angle + angle;

                DistanceHistogrammBeam newBeam = new DistanceHistogrammBeam(pt1, pt2, beamOrg.Distance, angleNew, i, rval);
                newBeam.P2Error = pt2Error;
                rval.Beams.Add(newBeam);
            }
            return rval;
        }
    
    
        //****************************
        public DistanceHistogramm Fusion(DistanceHistogramm add)
        {
            DistanceHistogramm rval = new DistanceHistogramm(AngleResolution, Range);

            int i = 0, j = 0, count = 0;
            do
            {
                DistanceHistogrammBeam next = Beams[i];
                if (next.Angle <= add.Beams[j].Angle)
                    i++;
                else
                {
                    next = add.Beams[j];
                    j++;
                }
                rval.Beams.Add(next.FusionCopy(count, rval));
                count++;


            } while (i < this.Beams.Count && j < add.Beams.Count);

            //Rest of i
            if (i < this.Beams.Count)
                for (; i < Beams.Count; i++)
                {
                    rval.Beams.Add(Beams[i].FusionCopy(count, rval));
                    count++;
                }


            //Rest of j
            if (j < add.Beams.Count)
                for (; j < Beams.Count; j++)
                    rval.Beams.Add(add.Beams[j].FusionCopy(count, rval));

            return rval;
        }


        public double Area
        {
            get
            {
                double rval = 0;
                for (int i = 0; i < Beams.Count - 1; i++)
                {
                    double angle1 = Beams[i].Angle;
                    double angle2 = Beams[i].Next.Angle;
                    if (angle2 < angle1)
                        angle2 += Math.PI * 2;
                    double dAngle = angle2 - angle1;
                    double min = Math.Min(Beams[i].Next.Distance, Beams[i].Distance);
                    double diff = Math.Abs(Beams[i].Next.Distance - Beams[i].Distance);
                    double area = dAngle * (min + diff / 2);
                    rval += area;
                }
                return rval;
            }

        }

        public double AreaSquare
        {
            get
            {
                double rval = 0;

                double average = 0;

                for (int i = 0; i < Beams.Count - 1; i++)
                    average += Beams[i].Distance;

                average = average / Beams.Count;

                for (int i = 0; i < Beams.Count - 1; i++)
                {
                    double angle1 = Beams[i].Angle;
                    double angle2 = Beams[i].Next.Angle;
                    if (angle2 < angle1)
                        angle2 += Math.PI * 2;
                    double dAngle = angle2 - angle1;
                    double dist1 = Beams[i].Next.Distance - average;
                    double dist2 = Beams[i].Distance - average;

                    double min = Math.Min(dist1, dist2);
                    double diff = Math.Abs(Beams[i].Next.Distance - Beams[i].Distance);

                    double area = dAngle * (min + diff / 2);
                    rval += area * area;
                }
                return rval;
            }

        }


        public DistanceHistogramm ShiftCenter(PointCoordinate offset, double angle)
        {
            DistanceHistogramm rval = Transform(new PointCoordinate(0, 0), 0);
            for (int i = 0; i < rval.Beams.Count; i++)
            {
                DistanceHistogrammBeam beam = rval.Beams[i];
                beam.P1 = offset;
                if (beam.P2Error != null)
                {
                    beam.Distance = DistanceBetweenPoints(beam.P1, beam.P2Error);
                    beam.Angle = AngleOfLine(new Line(beam.P1, beam.P2Error));
                }
            }
            rval.ReorderByAngle();
            return rval;
        }



        public Polygon GetPolygon()
        {
            List<PointCoordinate> pointList = new List<PointCoordinate>();

            foreach (DistanceHistogrammBeam beam in this.Beams)
            {
                if (beam.P2Error != null)
                    pointList.Add(beam.P2Error);

            }
            PointCoordinate[] pointArray = pointList.ToArray();
            Polygon poly = new Polygon(pointArray);

            return poly;
        }

        private void ReorderByAngle()
        {
            SortedList<double, DistanceHistogrammBeam> sortList = new SortedList<double, DistanceHistogrammBeam>();
            foreach (DistanceHistogrammBeam beam in this.Beams)
            {
                while (sortList.ContainsKey(beam.Angle))
                    beam.Angle += 0.0000000001;
                sortList.Add(beam.Angle, beam);
            }
            this.Beams.Clear();
            int index = 0;
            foreach (KeyValuePair<double, DistanceHistogrammBeam> kvp in sortList)
            {
                kvp.Value.Index = index;
                this.Beams.Add(kvp.Value);
                index++;
            }


        }



        //****************************



    }

    public enum SignificantSignType
    { 
        Constant = 0,
        Rising = 1,
        Falling = 2
    }
 
   



    public class SignificantSign
    {
        public SignificantSign(double angle)
        {
            Angle = angle;
        }


        private Angle angle = new Angle(0);
        public double Angle
        {
            get { return angle.Radian; }
            set { angle.Radian = value; }
        }

        public double Value
        {
            get 
            {
                double sum = 0;
                foreach (double d in values)
                    sum += d;
                return (sum / values.Count);
            }
        }

        public int Width {get{return values.Count;} }
        public SignificantSignType Type;
        public List<double> values = new List<double>();
        public double C_JumpThreshold = 50;
        public double C_Linethreshold = 30;

        public bool Add(double val, double angle)
        {
            if (values.Count == 0)
            {
                values.Add (val);
                Angle = angle;
                return true;
            }

            double diff = values[0] - val;
            if (values.Count == 1)
            {
                //Sudden jump?
                if (System.Math.Abs(diff) > C_JumpThreshold)
                {
                    Type = (diff < 0 ? SignificantSignType.Falling : SignificantSignType.Rising);
                    return false;
                }
                else
                {
                    values.Add(val);
                    return true;
                }
            }


            double m = (diff) / values.Count;
            double b = values[0];
            bool bMatch = true;
            for (int i = 1; i < values.Count; i++)
            {
                double y = m * i + b;
                if (System.Math.Abs(values[i] - y) > C_Linethreshold)
                {
                    bMatch = false;
                    break;
                }
            }

            if (bMatch)
            {
                values.Add(val);
                Angle = angle;
                if (System.Math.Abs(diff) < C_Linethreshold)
                    Type = SignificantSignType.Constant;
                else
                    Type = (diff < 0 ? SignificantSignType.Falling : SignificantSignType.Rising);   
                return true;
            }
            else
                return false;
        }
    
    }


    public class DistanceHistogrammBeam
    {
        public double Distance;
        public int Index;
        public double Angle;
        public DistanceHistogramm Histogramm;
        public PointCoordinate P1, P2, P2Error;
        
        public DistanceHistogrammBeam(PointCoordinate p1, PointCoordinate p2, double distance, double angle, int index, DistanceHistogramm histogramm)
        {
            P1 = p1;
            P2 = p2;
            if (p2 != null)
                P2Error = new PointCoordinate(P1.X + Math.Cos(angle) * distance, P1.Y + Math.Sin(angle) * distance);
            
            Distance = distance;
            Index = index;
            Angle = angle;
            Histogramm = histogramm; 
        }


        public int AngleDegree
        {
            get { return System.Convert.ToInt32(Angle * 180 / System.Math.PI); }
        }



        public double D_Distance 
        {
            get
            {
                    return (Distance - Previous.Distance);
            }
        }

        public double D2_Distance
        {
            get
            {
                return D_Distance - Previous.D_Distance;  
            }
        }


        internal DistanceHistogrammBeam FusionCopy(int index, DistanceHistogramm histogramm)
        {
            DistanceHistogrammBeam rval = new DistanceHistogrammBeam(this.P1, this.P2, this.Distance, this.Angle, index, histogramm);
            rval.P2Error = this.P2Error;
            return rval;
        }

        internal DistanceHistogrammBeam Previous
        {
            get
            {
                if (Index == 0)
                    return Histogramm.Beams[Histogramm.Beams.Count - 1];
                else
                    return Histogramm.Beams[Index - 1];
            }
        }

        internal DistanceHistogrammBeam Next
        {
            get
            {
                if (Index == Histogramm.Beams.Count - 1)
                    return Histogramm.Beams[0];
                else
                    return Histogramm.Beams[Index + 1];
            }
        }

        public bool IsOutOfRange { get { return (Distance >= Histogramm.Range); } }

        public bool IsMaximum
        {
            get
            {
                return (D_Distance > 0 && Next.D_Distance <= 0);
            }
        }

        public bool IsMinimum
        {
            get
            {
                return (D_Distance < 0 && Next.D_Distance >= 0);
            }
        }

    
    }


}
