﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;

namespace _2DTest
{
    public interface IBotsWorld
    {
        DistanceHistogramm CreateDistanceHistogramm(PointCoordinate pt, double startAngle, BeamSettings beamSettings);
        //WorldSettings worldSettings;
    }
}
