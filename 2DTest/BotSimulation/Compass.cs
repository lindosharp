﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    class Compass
    {
        Bot bot = null;
        double resolutionRad;
        int resolutionDegree;
        public Compass(int angleResolution, Bot theBot)
        {
            bot = theBot;
            resolutionDegree = angleResolution; 
            resolutionRad = (double)angleResolution * Math.PI / 180; 
        }

        public void Correct()
        { 
            double x = 180 * bot.RealPosition.Angle / (Math.PI);
            x= Math.Round(x/ resolutionDegree);
            
            double y = 180 * bot.DrivePosition.Angle / (Math.PI);
            y= Math.Round(y/ resolutionDegree);
            if (x!=y)
            {
                bot.DrivePosition.Angle = (double)x * resolutionDegree  * Math.PI / 180; 
            }
        }
    }
}
