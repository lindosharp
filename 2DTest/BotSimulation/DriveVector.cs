﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;
using System.ComponentModel;


namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class DriveVector
    {
        /// <summary>
        /// Creates a new instance of the vector with angle = 0 and Point = (0,0)
        /// </summary>
        public DriveVector()
        {
            Angle = 0;
            Point = new PointCoordinate (0,0);
        }



        /// <summary>
        /// Creates a new instance of the vector
        /// </summary>
        /// <param name="angle">value for Angle property</param>
        /// <param name="pt">value for Point property</param>
        public DriveVector(double angle, PointCoordinate pt)
        {
            Angle = angle;
            Point = pt;
        }


        public static DriveVector operator +(DriveVector a, DriveVector b)
        {
            double distanceB = System.Math.Sqrt((b.Point.X * b.Point.X) + (b.Point.Y * b.Point.Y));

            double diffX = System.Math.Cos(a.Angle) * distanceB;
            double diffY = System.Math.Sin(a.Angle) * distanceB;
            if (b.Point.X < 0)
                diffX = -diffX;

            //if (b.Point.Y < 0)
            //    diffY = -diffY;

            double X = a.Point.X + diffX;
            double Y = a.Point.Y + diffY;
            
            double angle = a.Angle + b.Angle;

            DriveVector retVal = new DriveVector(angle, new PointCoordinate(X, Y));
            return retVal;
        }


        /// <summary>
        /// Described where the bot looks to
        /// </summary>
        public double Angle { get; set; }

        public double Degree { get { return Angle * 180 / System.Math.PI; } }

        /// <summary>
        /// Describes where the bot is
        /// </summary>
        public PointCoordinate  Point { get; set; }

        public override string ToString()
        {
            return Degree.ToString("0.00") + " {" + Point.X.ToString("0.00") + ": " + Point.Y.ToString("0.00") + "}";
        }

    }
}
