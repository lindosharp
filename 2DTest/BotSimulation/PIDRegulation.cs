﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    class PIDRegulation
    {
        public PIDRegulation()
        {
            RecreateBuffer();
        }
        
        private void RecreateBuffer()
        {
            double[] BufferNew = new double[Depth];
            double[] BufferDeadCyclesRetNew = new double[Depth];

            if (Buffer != null)
            {
                Array.Copy(Buffer, BufferNew, Math.Min(Buffer.Length, BufferNew.Length));
                Array.Copy(BufferDeadCyclesRet, BufferDeadCyclesRetNew, Math.Min(Buffer.Length, BufferNew.Length));
            }

            Buffer = BufferNew;
            BufferDeadCyclesRet = BufferDeadCyclesRetNew;

            if (iBuffer > Buffer.Length - 1)
                iBuffer = Buffer.Length - 1;

        }


        public double KP=0.001;
        public double KI = 0.0001;
        public double KD =0.001;



        private int depth = 10;
        public int Depth
        {
            get { return depth; }
            set
            {
                depth = value;
                RecreateBuffer(); 
            }
        }

        public int DeadCycles=0;
        //public int DCI = 1; //Death cycle for I reaction
        //public int DCD = 0; //Death cycle for D reaction

        private double[] Buffer;
        private double[] BufferDeadCyclesRet;
        private int iBuffer = 0;
        
        private double Sum = 0;
        

        public double Calculate(double diff)
        {
            //Proportional
            double p = diff * KP;

            //Differential
            double previous = Buffer[iBuffer];
            double d = (diff - previous) * KD;

            //Integral
            iBuffer = (iBuffer + 1) % Buffer.Length;
            double oldVal = Buffer[iBuffer];
            Buffer[iBuffer] = diff;
            Sum -= oldVal;
            Sum += diff;
            double i = Sum/ Depth * KI;

            double all = -1 * (p + i + d);
            BufferDeadCyclesRet[iBuffer] = all;
            double rval = BufferDeadCyclesRet[(iBuffer + BufferDeadCyclesRet.Length - DeadCycles) % BufferDeadCyclesRet.Length];   
            
            return rval;
        }

    }
}
