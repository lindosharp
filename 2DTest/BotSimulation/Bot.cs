﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;
using FormsTooling.Randomize;
using _2DTest.lindo;

namespace _2DTest
{
    public enum DriveCommandDirection
    {
        Forward = 0,
        Back = 1,
        TurnLeft = 2,
        TurnRight = 3
    }


    public enum DriveCommandState
    {
        /// <summary>
        /// Executed successful
        /// </summary>
        Complete = 0,

        /// <summary>
        /// Still running
        /// </summary>
        Running = 1,
        /// <summary>
        /// Aborted by bumping at wall
        /// </summary>
        Bumping = 2,
        /// <summary>
        /// Aborted by DriveCommandProgress.Cancel
        /// </summary>
        Aborted = 3
    }

    public class Bot : FormsTooling._2D.Drawing.IDrawObject
    {
        public BotSettings settings;


        /// <summary>
        /// Relative positition in one Drive sequenz (subdivided by CalculationDivider)
        /// </summary>
        public DriveVector CurrentDriveCommandPosition = new DriveVector();

        /// <summary>
        /// Position recorded by Bot (should be corrected from map data)
        /// </summary>
        public DriveVector DrivePosition;


        /// <summary>
        /// Position in real world
        /// </summary>
        public DriveVector RealPosition;
        IBotsWorld world = null; //Nice phil. comparison: YOU NEED THE WORLD BUT THE WRLD DO NOT NEED YOU.

        Compass compass = null;

        public Bot(BotSettings botSettings, IBotsWorld botsWorld)
        {
            settings = botSettings;
            world = botsWorld;
            compass = new Compass(2, this);

            drawCircle = new FormsTooling._2D.Drawing.DrawCircle(new PointCoordinate(0, 0), settings.Diameter / 2, System.Drawing.Color.Red);
            drawLine = new FormsTooling._2D.Drawing.DrawLine(new PointCoordinate(0, 0), new PointCoordinate(0, 0), System.Drawing.Color.Red);
            shadowCircle = new FormsTooling._2D.Drawing.DrawCircle(new PointCoordinate(0, 0), settings.Diameter / 2, System.Drawing.Color.Fuchsia);
            shadowLine = new FormsTooling._2D.Drawing.DrawLine(new PointCoordinate(0, 0), new PointCoordinate(0, 0), System.Drawing.Color.Fuchsia);
            AddBotDrawCommands();



            DrivePosition = new DriveVector();

        }


        /// <summary>
        /// Exploration Macro: Drive 
        /// </summary>
        /// <returns>true, if the bot changed his x,y-position by driving forward</returns>
        public bool RightDriveRule()
        {
            bool rVal = true;
            bool bFree = false;
            PointCoordinate PreviousPos = this.DrivePosition.Point;



            do
            {
                bFree = DriveCommand(30, DriveCommandDirection.Forward);
                TurnCommand(-Math.PI / 8);
            } while (bFree);
            TurnCommand(Math.PI / 2);
            rVal = !PreviousPos.Compare(this.DrivePosition.Point);
            return rVal;

        }



        private void AddBotDrawCommands()
        {
            DrawCommands.Add(shadowLine);
            DrawCommands.Add(drawCircle);
            DrawCommands.Add(drawLine);
            DrawCommands.Add(shadowCircle);
        }

        internal bool WallDrive(double Length, lindo.ScoutBridge scoutCorrector, lindo.ScoutBridge scoutRecorder)
        {
            double sum = 0;
            lastDistance = 0;
            lastPoint = this.DrivePosition.Point;


            PIDRegulation regDistance = new PIDRegulation();
            regDistance.KP = -0.0004;
            regDistance.KI = -0.00002;
            regDistance.KD = 0;


            PIDRegulation regAngle = new PIDRegulation();
            regAngle.KP = 0.003;
            regAngle.KI = 0.0002;
            regAngle.KD = 0.0002;
            regAngle.DeadCycles = 1;

            PIDRegulation regForwardDistance = new PIDRegulation();
            regForwardDistance.Depth = 3;
            regForwardDistance.KP = -0.01;
            regForwardDistance.KI = -0.00;
            regForwardDistance.KD = -0.00;


            while (sum < Length)
            {
                DistanceHistogramm hist = GetDistanceHistogramm();
                hist = hist.Normalize();

                DistanceHistogrammBeam beam270 = hist.Beams[(hist.Beams.Count * 27) / 36];
                DistanceHistogrammBeam beam280 = hist.Beams[(hist.Beams.Count * 28) / 36];
                DistanceHistogrammBeam beam260 = hist.Beams[(hist.Beams.Count * 26) / 36];
                DistanceHistogrammBeam beam0 = hist.Beams[0];

                double diffDistance = 90 - beam270.Distance;
                double diff45 = Math.Min(beam280.Distance - beam260.Distance, 100);
                diff45 = Math.Max(diff45, -100);

                double Diff0 = System.Math.Max(150 - beam0.Distance, 0);

                double angle = regAngle.Calculate(diff45) + regDistance.Calculate(diffDistance) + regForwardDistance.Calculate(Diff0);
                TurnCommand(angle);

                bool success = DriveCommand(10, DriveCommandDirection.Forward);
                sum += this.CurrentDriveCommandPosition.Point.X;

                CorrectScout(scoutCorrector, sum);
                AccumulateScout(scoutRecorder, sum);

                if (success == false)
                    TurnCommand(30);

            }
            this.DrawCommands.Clear();
            AddBotDrawCommands();

            return true;
        }


        double lastDistance = -500;
        PointCoordinate lastPoint = null;
        private void CorrectScout(lindo.ScoutBridge scout, double distance)
        {
            if (scout == null || distance - lastDistance <= 50)
            {
                return;
            }
            DistanceHistogramm histogram = GetDistanceHistogramm(RealPosition).Normalize();
            scout.addBeams(histogram.Beams, RealPosition.Point, 0);
            //Correct position
            lindo.Motion motionBest = scout.MotionBest;
            if (null != motionBest && null != lastPoint)
            {
                DrivePosition.Point = Transformator.minus(lastPoint, Transformator.getPoint(motionBest.offset));
                DrivePosition.Angle = DrivePosition.Angle - motionBest.rotation;
                System.Diagnostics.Debug.WriteLine(PosCompare());
            }
            //Update drawing
            this.DrawCommands.Clear();
            AddBotDrawCommands();
            scout.draw(this.DrawCommands);

            lastDistance = distance;
            lastPoint = RealPosition.Point;
        }

        private void AccumulateScout(lindo.ScoutBridge scout, double distance)
        {
            //TODO
        }

        /// <summary>
        /// Turn to degree of longest distance
        /// </summary>
        /// <returns></returns>
        public bool Escape()
        {
            DistanceHistogramm histogramm = GetDistanceHistogramm();
            double maxDistance = 0;
            int MaxAngleDegree = 0;
            for (int i = 0; i < histogramm.Beams.Count; i++)
            {
                double distance = TestFreeCorridor(i, settings.Diameter + 20, histogramm);
                if (distance > maxDistance)
                {
                    maxDistance = distance;
                    MaxAngleDegree = i * settings.Beams.AngleResolution;
                }

            }

            TurnCommand(MaxAngleDegree);
            return DriveCommand(System.Convert.ToInt32(maxDistance - 20), DriveCommandDirection.Forward);
        }

        double TestFreeCorridor(int beamIndex, double width, DistanceHistogramm histogramm)
        {
            int beamCount = histogramm.Beams.Count;
            double maxValue = histogramm.Beams[beamIndex].Distance;

            //Test 90° Distances
            if (histogramm.Beams[(beamCount + beamIndex + beamCount / 4) % beamCount].Distance < width / 2)
                return 0;
            if (histogramm.Beams[(beamCount + beamIndex - beamCount / 4) % beamCount].Distance < width / 2)
                return 0;


            for (int i = 1; i < beamCount / 4; i++)
            {
                DistanceHistogrammBeam beamLeft = histogramm.Beams[(beamIndex - i + beamCount) % beamCount];
                DistanceHistogrammBeam beamRight = histogramm.Beams[(i + beamIndex) % beamCount];

                double angle = Math.PI * ((double)i * settings.Beams.AngleResolution / 180);

                if (Math.Sin(angle) * beamLeft.Distance < width / 2)
                {
                    double x = Math.Cos(angle) * beamLeft.Distance;
                    if (x < maxValue)
                        maxValue = x;
                }

                if (Math.Sin(angle) * beamRight.Distance < width / 2)
                {
                    double x = Math.Cos(angle) * beamLeft.Distance;
                    if (x < maxValue)
                        maxValue = x;
                }

            }

            return maxValue;
        }


        public bool TurnCommand(int degree)
        {
            double angle = (double)degree / 180 * Math.PI;
            return TurnCommand(angle);
        }


        public bool TurnCommand(double angle)
        {
            double U = System.Math.PI * settings.WheelDistance;

            // Normalize angle to -PI..PI
            while (angle > System.Math.PI)
            {
                angle -= 2 * System.Math.PI;
            }

            while (angle < -System.Math.PI)
            {
                angle += 2 * System.Math.PI;
            }

            DriveCommandDirection direction = DriveCommandDirection.TurnLeft;
            if (angle < 0)
            {
                direction = DriveCommandDirection.TurnRight;
                angle = -angle;
            }
            int steps = System.Convert.ToInt32((U * angle) / (2 * System.Math.PI));
            return DriveCommand(steps, direction);
        }

        public bool DriveCommand(int steps, DriveCommandDirection direction)
        {
            //Prepare Progress event
            DriveCommandProgressEventArgs progress = new DriveCommandProgressEventArgs();

            if (RealPosition == null)
                RealPosition = settings.StartPosition;

            CurrentDriveCommandPosition = new DriveVector();
            int counter = 0;
            int div = settings.CalculationDivider;
            while (counter < steps)
            {
                if (counter + div <= steps)
                    counter += div;
                else
                {
                    div = steps - counter;
                    counter = steps;
                }

                double left = div * ((direction == DriveCommandDirection.Forward) || (direction == DriveCommandDirection.TurnLeft) ? 1 : -1);
                double right = div * ((direction == DriveCommandDirection.Forward) || (direction == DriveCommandDirection.TurnRight) ? 1 : -1);
                DriveVector vecIdeal = CalculatePhysicsVector(left, right);

                //Static deviation
                left += (left * settings.DriveError.WheelDiameterDeviation / 2);
                right -= (right * settings.DriveError.WheelDiameterDeviation / 2);


                DriveError.CalcNoisyWaySlipMarker marker = DriveError.CalcNoisyWaySlipMarker.IsMid;
                if (counter == div)
                    marker |= DriveError.CalcNoisyWaySlipMarker.IsFirst;
                if (counter == steps)
                {
                    marker |= DriveError.CalcNoisyWaySlipMarker.IsLast;
                    progress.State = DriveCommandState.Complete;
                }
                settings.DriveError.CalcNoisyWays(ref left, ref right, marker);
                DriveVector vecReal = CalculatePhysicsVector(left, right);

                //Test for collision 
                double r = settings.Diameter / 2;
                DriveVector vecTest = RealPosition + vecReal;

                progress.Histogramm = world.CreateDistanceHistogramm(vecTest.Point, vecTest.Angle, settings.Beams);
                bool bBump = (direction == DriveCommandDirection.Forward || direction == DriveCommandDirection.Back ? CollisionWarn(progress.Histogramm) : false);

                if (bBump)
                {
                    progress.State = DriveCommandState.Bumping;
                }
                else
                {
                    CurrentDriveCommandPosition += vecIdeal;
                    DrivePosition += vecIdeal;
                    if (compass != null)
                        compass.Correct();

                    RealPosition += vecReal;

                }
                OnDriveCommandProgress(progress);
                SetDrawObjects();

                if (progress.Cancel || progress.State == DriveCommandState.Bumping)
                    return false;
            }
            return (true);
        }


        public string PosCompare()
        {
            return "POSITION COMPARE drive:" + DrivePosition.ToString() + "  real: " + RealPosition.ToString();
        }

        public event EventHandler<DriveCommandProgressEventArgs> DriveCommandProgress;

        protected void OnDriveCommandProgress(DriveCommandProgressEventArgs e)
        {
            if (DriveCommandProgress != null)
            {
                DriveCommandProgress(this, e);
            }

        }


        public DistanceHistogramm GetDistanceHistogramm()
        {
            return GetDistanceHistogramm(RealPosition).Normalize().Transform(DrivePosition.Point, DrivePosition.Angle);
        }


        #region internals
        FormsTooling._2D.Drawing.DrawCircle drawCircle;
        FormsTooling._2D.Drawing.DrawLine drawLine;
        FormsTooling._2D.Drawing.DrawCircle shadowCircle;
        FormsTooling._2D.Drawing.DrawLine shadowLine;

        private void SetDrawObjects()
        {
            drawCircle.Center = RealPosition.Point;
            drawLine.P1 = drawCircle.Center;
            drawLine.P2 = new PointCoordinate(drawLine.P1.X + drawCircle.Radius * System.Math.Cos(RealPosition.Angle),
                    drawLine.P1.Y + drawCircle.Radius * System.Math.Sin(RealPosition.Angle));

            shadowCircle.Center = DrivePosition.Point;
            shadowLine.P1 = shadowCircle.Center;
            shadowLine.P2 = new PointCoordinate(shadowLine.P1.X + shadowCircle.Radius * System.Math.Cos(DrivePosition.Angle),
                    shadowLine.P1.Y + shadowCircle.Radius * System.Math.Sin(DrivePosition.Angle));

        }


        private DistanceHistogramm GetDistanceHistogramm(DriveVector pos)
        {
            return world.CreateDistanceHistogramm(pos.Point, pos.Angle, settings.Beams);
        }


        private bool CollisionWarn(DistanceHistogramm histogramm)
        {
            double C_SecurityDistance = 5;
            bool rVal = false;
            double radius = settings.Diameter / 2 + C_SecurityDistance;
            foreach (DistanceHistogrammBeam beam in histogramm.Beams)
            {
                if (beam.Distance <= radius)
                {
                    rVal = true;
                    break;
                }
            }
            return rVal;
        }

        private DriveVector CalculatePhysicsVector(double distanceLeft, double distanceRight)
        {
            //Infinitive radius -> linear movement
            if (distanceLeft == distanceRight)
            {
                DriveVector retVal = new DriveVector(0, new PointCoordinate(distanceLeft, 0));
                return retVal;
            }

            //circular movement
            double r = 0;
            double rLeft = 0, rRight = 0;


            r = CalculateVirtualRadius(distanceLeft, distanceRight);
            rLeft = r + settings.WheelDistance;
            rRight = r;

            double alfaLeft = distanceLeft / (rLeft);
            double alfaRight = distanceRight / (rRight);

            if (double.IsNaN(alfaLeft))
                alfaLeft = alfaRight;
            if (double.IsNaN(alfaRight))
                alfaRight = alfaLeft;
            double degreeLeft = alfaLeft * 360 / (2 * System.Math.PI);

            double yLeft = rLeft * (1 - System.Math.Cos(alfaLeft));
            double yRight = (rRight * (1 - System.Math.Cos(alfaRight)));

            double xLeft = rLeft * System.Math.Sin(alfaLeft);
            double xRight = (rRight * System.Math.Sin(alfaRight));

            double xCenter = (xLeft + xRight) / 2;
            double yCenter = (yLeft + yRight) / 2;

            double alfa = alfaLeft;
            double alfaDegree = alfa * 360 / (2 * System.Math.PI);

            return new DriveVector(alfa, new PointCoordinate(xCenter, yCenter));
        }

        /// <summary>
        /// r= k*x2/(x1-x2)
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        private double CalculateVirtualRadius(double x1, double x2)
        {
            if (x1 == x2)
                return double.MaxValue;
            double r = settings.WheelDistance * x2 / (x1 - x2);
            return r;
        }

        #endregion

        private List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands = new List<FormsTooling._2D.Drawing.IDrawCommand>();
        public List<FormsTooling._2D.Drawing.IDrawCommand> DrawCommands { get { return drawCommands; } private set { drawCommands = value; } }
    }



    public class DriveCommandProgressEventArgs : System.ComponentModel.CancelEventArgs
    {
        public DriveCommandState State = DriveCommandState.Running;
        public DistanceHistogramm Histogramm;
    }
}
