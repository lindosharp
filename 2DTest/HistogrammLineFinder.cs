﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest
{
    class HistogrammLineFinder
    {
        static int angleResolution = 0;
        static List<AngleEntry> angleTable = new List<AngleEntry>(); 



        public static int AngleResolution
        {
            get {return angleResolution;}
            set 
            {
                if (angleResolution == value)
                    return;

                angleResolution = value;
                angleTable = new List<AngleEntry>();

                for (int i = -90  ; i < 90; i += angleResolution)
                {
                    AngleEntry entry = new AngleEntry();
                    entry.Angle = i;
                    double AngleMax = (double)i + (i==90 ? 0 :(double)angleResolution / 2);
                    double AngleMin = (double)i - (i==-90 ? 0 : (double)angleResolution / 2);

                    double Limit1 = System.Math.Cos((AngleMax  + angleResolution)* 2 * System.Math.PI / 360) / System.Math.Cos((AngleMax ) * 2 * System.Math.PI / 360);
                    double Limit2 = System.Math.Cos((AngleMin  + angleResolution) * 2 * System.Math.PI / 360) / System.Math.Cos((AngleMin) * 2 * System.Math.PI / 360);
                    entry.Max = System.Math.Max(Limit1, Limit2);
                    entry.Min = System.Math.Min(Limit1, Limit2);
                    
                    angleTable.Add(entry);
                    //System.Diagnostics.Debug.WriteLine(entry.Angle + " Max: " + entry.Max + " Min: " + entry.Min);  
                }
            }
        }



        public static void GetAngleRange(double divMax, double divMin, ref int AngleMax, ref int AngleMin)
        {
            AngleMax = GetAngle(divMax);
            AngleMin = GetAngle(divMin);
        }


        public static int GetAngle(double div)
        {
            if (div < 0)
                return -90;

            int index = angleTable.Count / 2;
            int previousIndex = angleTable.Count;
            AngleEntry entry = null;

            while (true)
            {
                entry = angleTable[index];
                int result = entry.Compare(div);
                if (result == 0)
                {
                    break;
                }

                int jumpWidth = System.Math.Abs(previousIndex - index)/2;
                if (jumpWidth == 0)
                    jumpWidth = 1;
                previousIndex = index;
                index += result * jumpWidth;
                

            }
            return entry.Angle; 
        }

    
        public List<PlausibleLine> TestHistogramm(DistanceHistogramm histogramm, int angleResolution)
        {
            List<PlausibleLine> lines = new List<PlausibleLine>();
 
            PlausibleLine line = new PlausibleLine(angleResolution);


            for (int i = 0; i < histogramm.Beams.Count; i++)
            {
                DistanceHistogrammBeam beam = histogramm.Beams[i];
                while (line.Add(beam) == true) 
                {
                    beam = beam.Next; 
                }
                if (line.Width >= 5)
                {
                    lines.Add(line);
                    i += line.Width - 1;
                    line = new PlausibleLine(angleResolution);
                }
                else
                    line.values.Clear(); 
            }
            //    foreach (DistanceHistogrammBeam beam in histogramm.Beams)
            //    {
            //        if (line.Add(beam.Distance, beam.Angle) == false)
            //        {
            //            if (line.Width > 5)
            //                lines.Add(line);
            //            line = new PlausibleLine(beam.Angle, angleResolution);
            //        }
            //    }
            //if (line.Width > 5)
            //    lines.Add(line);

            return lines;
        
        }
    
    
    }




    class AngleEntry
    {
        public double Max;
        public double Min;
        public int Angle;

        public int Compare(double div)
        {
            if (div <= Min)
                return 1;
            if (div > Max)
                return -1;
            return 0;
        }
    }

    public class PlausibleLine
    {
        public int AngleResolution = 0;
        public double BeamStartAngle;
        public int Width { get { return values.Count; } }
        public List<DistanceHistogrammBeam> values = new List<DistanceHistogrammBeam>();

        public int LineAngleMax = 0;
        public int LineAngleMin = 0;


        public PlausibleLine(int angleResolution)
        {
            AngleResolution = angleResolution; 
        }




        // Verhältnis der beiden Distanzen bilden
        // entsprechenden Index aus Tabelle suchen
        //ACHTUNG NOISE IST DYNAMISCH IN DIE BERECHUNG EINZUBEZIEHEN        

        int lastMin = -90;
        int lastMax = 90;

        public bool Add(DistanceHistogrammBeam beam)
        {
            if (values.Count == 0)
            {
                values.Add(beam);
                BeamStartAngle = beam.AngleDegree;
                return true;
            }




            double C_Noise = 3;
            double dist1Max = values[Width - 1].Distance  + C_Noise;
            double dist1Min = values[Width - 1].Distance - C_Noise;
            double dist2Max = beam.Distance  + C_Noise;
            double dist2Min = beam.Distance - C_Noise;

            if (dist1Max - dist2Min > 50)
                return false;


            double dx1 = dist2Max / dist1Min;
            double dx2 = dist2Min / dist1Max;

            int angle1 = HistogrammLineFinder.GetAngle(dx1);
            int angle2 = HistogrammLineFinder.GetAngle(dx2);
            int angleMin = System.Math.Min(angle1, angle2);
            int angleMax = System.Math.Max(angle1, angle2);


            if (values.Count == 1)
            {
                values.Add(beam);
                lastMin = angleMin;
                lastMax = angleMax;
                return true;
            }


            

            bool bMatch = true;
            //Teste hier
            if ((angleMin > (lastMax - AngleResolution)) || (angleMax < (lastMin - AngleResolution)))
                bMatch = false;
            else //Schnittmenge berechnen
            {
                lastMax = System.Math.Min(lastMax - AngleResolution, angleMax);
                lastMin = System.Math.Max(lastMin - AngleResolution, angleMin);
            }

            if (bMatch)
            {
                values.Add(beam);
                //System.Diagnostics.Debug.WriteLine(values.Count);   
                return true;
            }
            else
                return false;
        }

    }

}
