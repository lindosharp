﻿using System;
using System.Collections.Generic;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Find shortest route from origin to target (dijkstra). */
    public class RouterTwo : Router
    {
        override public ISet<Route> findRoutes(PointCoordinate origin, PointCoordinate target, List<PointCoordinate> points)
        {
            routes = new HashSet<Route>();
            if (null == origin)
            {
                return routes;
            }
            HashSet<Route> borders = getBorders(points);
            List<Route> unvisited = new List<Route>();
            // start from origin
            unvisited.Add(new Route(origin, resolution));
            Route targetRoute = null == target ? null : new Route(target, resolution);
            RouteComparer comparer = new RouteComparer();
            for (int k = 0; k < iterations; k++)
            {
                if (0 == unvisited.Count)
                {
                    // all nodes visited
                    break;
                }
                unvisited.Sort(comparer);
                // no shorter route to this node exists because all other routes would contain an unvisited node to which the route is already longer
                Route route = unvisited[0];
                if (route.Equals(targetRoute))
                {
                    // shortest route to target found
                    routes.Clear();
                    routes.Add(route);
                    break;
                }
                unvisited.RemoveAt(0);
                routes.Add(route);
                foreach (Route neighbour in getNeighbours(route))
                {
                    if (routes.Contains(neighbour) || borders.Contains(neighbour))
                    {
                        continue;
                    }
                    int index = unvisited.IndexOf(neighbour);
                    if (-1 == index)
                    {
                        unvisited.Add(neighbour);
                    }
                    else
                    {
                        Route neighbourUnvisited = unvisited[index];
                        if (neighbour.distance < neighbourUnvisited.distance)
                        {
                            //Console.WriteLine("replace " + neighbour);
                            //Console.WriteLine(neighbour.distance + " < " + neighbourUnvisited.distance);
                            // replace previous route to neighbour because current route is shorter
                            unvisited.RemoveAt(index);
                            unvisited.Add(neighbour);
                        }
                    }
                }
            }
            return routes;
        }
    }

    /** Compare two routes by distance. */
    class RouteComparer : Comparer<Route>
    {
        public override int Compare(Route one, Route two)
        {
            return Math.Sign(one.distance - two.distance);
        }
    }
}
