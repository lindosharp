﻿using System;
using System.Collections.Generic;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Find any route from origin to target (poor man's dijkstra). */
    public class RouterOne : Router
    {
        override public ISet<Route> findRoutes(PointCoordinate origin, PointCoordinate target, List<PointCoordinate> points)
        {
            routes = new HashSet<Route>();
            if (null == origin)
            {
                return routes;
            }
            HashSet<Route> borders = getBorders(points);
            List<Route> unvisited = new List<Route>();
            // start from origin
            unvisited.Add(new Route(origin, resolution));
            Route targetRoute = null == target ? null : new Route(target, resolution);
            for (int k = 0; k < iterations; k++)
            {
                if (unvisited.Count == k)
                {
                    // all nodes visited
                    break;
                }
                Route route = unvisited[k];
                if (route.Equals(targetRoute))
                {
                    // route to target found
                    routes.Clear();
                    routes.Add(route);
                    break;
                }
                routes.Add(route);
                foreach (Route neighbour in getNeighbours(route))
                {
                    if (routes.Contains(neighbour) || borders.Contains(neighbour) || unvisited.Contains(neighbour))
                    {
                        continue;
                    }
                    unvisited.Add(neighbour);
                }
            }
            return routes;
        }
    }
}
