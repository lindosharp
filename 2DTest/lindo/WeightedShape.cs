﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Shape with weight (quality). */
    class WeightedShape
    {
        public Shape shape = new Shape();
        public double weight = 0;

        /* Default constructor. */
        public WeightedShape() { }

        /* Full constructor. */
        public WeightedShape(Shape shapeOne, double weightOne)
        {
            shape = shapeOne;
            weight = weightOne;
        }

        public override string ToString()
        {
            return "WeightedShape(" + shape + ", " + weight + ")";
        }
    }
}
