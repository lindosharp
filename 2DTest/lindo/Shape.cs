﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** List of polar coordinates sorted by angle. */
    public class Shape
    {
        /** Points sorted by angle. */
        public SortedList<double, Point> pointsByAngle = new SortedList<double, Point>();
        /** Points ordered by addition (first one wins). */
        public List<Point> pointsOrdered = new List<Point>();

        /** Add point to shape. */
        public void addPoint(Point point)
        {
            if (null != point)
            {
                if (!pointsByAngle.ContainsKey(point.angle))
                {
                    pointsByAngle.Add(point.angle, point);
                    pointsOrdered.Add(point);
                }
            }
        }

        /** Remove all points. */
        public void clear()
        {
            pointsByAngle.Clear();
            pointsOrdered.Clear();
        }

        override public string ToString()
        {
            String value = "Shape(";
            foreach (Point point in pointsByAngle.Values)
            {
                value += point + ", ";
            }
            return value + ")";
        }
    }
}
