﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Find the closest point of shapeTwo to the line between a point of shapeOne and the following point of shapeOne. */
    class ComparatorOne : Comparator
    {
        public double compare(Shape shapeOne, Shape shapeTwo)
        {
            double weight = 0;
            int weightCount = 1;
            Point pointOneMin = null;
            int i = 0;
            foreach (Point pointOneMax in shapeOne.pointsByAngle.Values)
            {
                if (null != pointOneMin)
                {
                    double weightMax = 0;
                    for (; i < shapeTwo.pointsByAngle.Count(); i++)
                    {
                        Point pointTwo = shapeTwo.pointsByAngle.Values.ElementAt(i);
                        if (pointTwo.angle < pointOneMin.angle)
                        {
                            continue;
                        }
                        if (pointTwo.angle > pointOneMax.angle)
                        {
                            break;
                        }
                        // between pointOneMin and pointOneMax
                        // use linear approximation: radiusOneDelta / angleOneDelta = radiusTwoDelta / angleTwoDelta 
                        double angleOneDelta = pointOneMax.angle - pointOneMin.angle;
                        double radiusOneDelta = pointOneMax.radius - pointOneMin.radius;
                        double angleTwoDelta = pointTwo.angle - pointOneMin.angle;
                        double radiusExpected = pointOneMin.radius + radiusOneDelta * angleTwoDelta / angleOneDelta;
                        double weightTemp = 1 - Math.Abs(radiusExpected - pointTwo.radius) / Math.Max(radiusExpected, pointTwo.radius);
                        //double weightTemp = 1.0 / (1.0 + Math.Abs(radiusExpected - pointTwo.radius));
                        if (weightTemp > weightMax)
                        {
                            weightMax = weightTemp;
                        }
                    }
                    weight += weightMax;
                    weightCount++;
                }
                pointOneMin = pointOneMax;
            }
            return weight / weightCount;
        }
    }
}
