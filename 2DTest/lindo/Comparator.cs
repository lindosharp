﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Compare two shapes and return the quality (0=terrible, 1=perfect). */
    interface Comparator
    {
        double compare(Shape shapeOne, Shape shapeTwo);
    }
}
