﻿using System;
using System.Collections.Generic;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Route (linked list of nodes with a distance. */
   public class Route
    {
        public double distance = 0;
        public Route predecessor = null;
        public int x = 0;
        public int y = 0;
        
        /** Full constructor. */
        public Route(int xOne, int yOne, Route predecessorOne)
        {
            x = xOne;
            y = yOne;
            setPredecessor(predecessorOne);
        }
            
        /** Full constructor. */
        public Route(PointCoordinate point, int resolution)
        {
            if (null != point)
            {
                x = ((int)point.X + resolution / 2) / resolution * resolution;
                y = ((int)point.Y + resolution / 2) / resolution * resolution;
            }
        }

        /** Set predecessor and update distance. */
        public void setPredecessor(Route predecessorOne)
        {
            if (null != predecessorOne)
            {
                predecessor = predecessorOne;
                int deltaX = x - predecessor.x;
                int deltaY = y - predecessor.y;
                distance = predecessor.distance + Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
            }
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals("" + obj);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return "Route(" + x + ", " + y + ")";
        }
    }
}
