﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Add point from shapeTwo and shapeOne up to limit points. */
    class AggregatorThree : Aggregator
    {
        public double angleMin = Math.PI / 90.0; // 2 degrees
        public double distanceMin = 16;
        public int limit = 1024;

        public WeightedShape aggregate(WeightedShape shapeOne, WeightedShape shapeTwo)
        {
            WeightedShape shape = new WeightedShape();
            for (int i = 0; i < shapeTwo.shape.pointsOrdered.Count && shape.shape.pointsOrdered.Count < limit; i++)
            {
                Point point = shapeTwo.shape.pointsOrdered[i];
                if (!isTooClose(point, shape.shape))
                {
                    shape.shape.addPoint(point);
                }
            }
            for (int i = 0; i < shapeOne.shape.pointsOrdered.Count && shape.shape.pointsOrdered.Count < limit; i++)
            {
                Point point = shapeOne.shape.pointsOrdered[i];
                if (!isTooClose(point, shape.shape))
                {
                    shape.shape.addPoint(point);
                }
            }
            shape.weight = (1.0 + shapeOne.weight + shapeTwo.weight) / 3.0;
            return shape;
        }

        /** Is pointOne too close to shape? */
        public bool isTooClose(Point pointOne, Shape shape)
        {
            double distanceMinSquare = distanceMin * distanceMin;
            foreach (Point pointTwo in shape.pointsOrdered)
            {
                if (Math.Abs(pointTwo.radius - pointOne.radius) < distanceMin
                    && Math.Abs(pointTwo.angle - pointOne.angle) < angleMin)
                {
                    // linear approximation
                    double deltaAngle = (pointTwo.angle - pointOne.angle) * pointOne.radius;
                    double deltaRadius = pointTwo.radius - pointOne.radius;
                    double distanceSquare = deltaAngle * deltaAngle + deltaRadius * deltaRadius;
                    if (distanceSquare < distanceMinSquare)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
