﻿using System;
using System.Collections.Generic;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Find routes. */
    public abstract class Router
    {
        protected internal int iterations = 9192;
        protected internal int resolution = 32;
        protected internal HashSet<Route> routes = new HashSet<Route>();

        /** Find route from origin to target (or all routes from origin if target is null). */
        public abstract ISet<Route> findRoutes(PointCoordinate origin, PointCoordinate target, List<PointCoordinate> points);

        /** Get points and neighbours. */
        protected HashSet<Route> getBorders(List<PointCoordinate> points)
        {
            HashSet<Route> borders = new HashSet<Route>();
            if (null != points)
            {
                foreach (PointCoordinate point in points)
                {
                    Route border = new Route(point, resolution);
                    borders.Add(border);
                    // first neighbours only
                    Route[] neighbours = getNeighbours(border);
                    for (int i = 0; i < 8; i++)
                    {
                        borders.Add(neighbours[i]);
                    }
                }
            }
            return borders;
        }

        /** Return 9 neighbours (grid centered around Route). */
        protected Route[] getNeighbours(Route route)
        {
            Route[] neighbours = new Route[16];
            // first
            neighbours[0] = new Route(route.x - resolution, route.y - resolution, route);
            neighbours[1] = new Route(route.x - resolution, route.y, route);
            neighbours[2] = new Route(route.x - resolution, route.y + resolution, route);
            neighbours[3] = new Route(route.x, route.y - resolution, route);
            neighbours[4] = new Route(route.x, route.y + resolution, route);
            neighbours[5] = new Route(route.x + resolution, route.y - resolution, route);
            neighbours[6] = new Route(route.x + resolution, route.y, route);
            neighbours[7] = new Route(route.x + resolution, route.y + resolution, route);
            // second
            neighbours[8] = new Route(route.x + 2 * resolution, route.y + resolution, route);
            neighbours[9] = new Route(route.x + 2 * resolution, route.y - resolution, route);
            neighbours[10] = new Route(route.x + resolution, route.y + 2 * resolution, route);
            neighbours[11] = new Route(route.x + resolution, route.y - 2 * resolution, route);
            neighbours[12] = new Route(route.x - resolution, route.y + 2 * resolution, route);
            neighbours[13] = new Route(route.x - resolution, route.y - 2 * resolution, route);
            neighbours[14] = new Route(route.x - 2 * resolution, route.y + resolution, route);
            neighbours[15] = new Route(route.x - 2 * resolution, route.y - resolution, route);
            return neighbours;
        }
    }

}
