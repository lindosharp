﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Motion from one point to another and rotation around the axis. */
    class Motion
    {
        public readonly Point offset;
        public readonly double rotation;

        /** Full constructor. */
        public Motion(Point offset, double rotation)
        {
            this.offset = offset;
            this.rotation = rotation;
        }

        /** Full constructor. */
        public Motion(double x, double y, double rotation)
        {
            offset = new Point(x, y);
            this.rotation = rotation;
        }

        override public string ToString()
        {
            return "Motion(" + offset + ", " + rotation + ")";
        }
    }
}
