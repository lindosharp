﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Create variations. */
    interface Variator
    {
        /** Create motions arbitrary distributed around the specified motion. */ 
        List<Motion> vary(Motion motion);
    }
}
