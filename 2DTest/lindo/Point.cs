﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Polar coordinate. */
    public class Point
    {
        public readonly double angle;
        public readonly double radius;

        /** Full constructor. */
        public Point(double angle, double radius)
        {
            this.angle = angle;
            this.radius = radius;
        }

        override public string ToString()
        {
            return "Point(" + angle + ", " + radius + ")";
        }
    }
}
