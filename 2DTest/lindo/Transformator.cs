﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Transform points and shapes. */
    abstract class Transformator
    {

        /** Get polar coordinates from (x,y) coordinates. */
        public static Point getPoint(double x, double y)
        {
            double angle = Math.Atan2(y, x);
            double radius = Math.Sqrt(x * x + y * y);
            return new Point(angle, radius);
        }

        /** Get polar coordinates from (x,y) coordinates. */
        public static Point getPoint(PointCoordinate point)
        {
            return null == point ? null : getPoint(point.X, point.Y);
        }

        /** Get (x,y) coordinates from polar coordinates. */
        public static PointCoordinate getPoint(Point point)
        {
            return null == point ? null : new PointCoordinate(getX(point), getY(point));
        }

        /** Get x coordinate from polar coordinates. */
        public static double getX(Point point)
        {
            return null == point ? 0 : Math.Cos(point.angle) * point.radius;
        }

        /** Get y coordinate from polar coordinates. */
        public static double getY(Point point)
        {
            return null == point ? 0 : Math.Sin(point.angle) * point.radius;
        }

        /** Move point by motion. */
        public static Point move(Point point, Motion motion)
        {
            if (null == point || null == motion)
            {
                return point;
            }
            double x = getX(point);
            double y = getY(point);
            if (null != motion.offset)
            {
                x += getX(motion.offset);
                y += getY(motion.offset);
            }
            point = getPoint(x, y);
            return new Point(point.angle + motion.rotation, point.radius);
        }

        /** Move shape by motion. */
        public static Shape move(Shape shapeOne, Motion motion)
        {
            if (null == shapeOne || null == motion)
            {
                return shapeOne;
            }
            Shape shapeTwo = new Shape();
            foreach (Point point in shapeOne.pointsOrdered)
            {
                shapeTwo.addPoint(move(point, motion));
            }
            return shapeTwo;
        }

        /** Move weightedShape by motion. */
        public static WeightedShape move(WeightedShape shape, Motion motion)
        {
            if (null == shape || null == motion)
            {
                return shape;
            }
            return new WeightedShape(move(shape.shape, motion), shape.weight);
        }

        /** Add offset to point (point.x + offset.x, point.y + offset.y). */
        public static PointCoordinate plus(PointCoordinate point, PointCoordinate offset)
        {
            return null == point || null == offset ? point : new PointCoordinate(point.X + offset.X, point.Y + offset.Y);
        }

        /** Substract offset from point (point.x - offset.x, point.y - offset.y). */
        public static PointCoordinate minus(PointCoordinate point, PointCoordinate offset)
        {
            return null == point || null == offset ? point : new PointCoordinate(point.X - offset.X, point.Y - offset.Y);
        }

        /** Revert route. */
        public static Route revert(Route route)
        {
            Route predecessorNew = route;
            if (null != route)
            {
                route = route.predecessor;
                predecessorNew.predecessor = null;
            }
            while (null != route)
            {
                Route predecessorOld = route.predecessor;
                route.predecessor = predecessorNew;
                predecessorNew = route;
                route = predecessorOld;
            }
            return predecessorNew;
        }
    }
}
