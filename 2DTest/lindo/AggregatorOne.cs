﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Add points and weights of shapeOne and shapeTwo. */
    class AggregatorOne : Aggregator
    {
        public WeightedShape aggregate(WeightedShape shapeOne, WeightedShape shapeTwo)
        {
            WeightedShape shape = new WeightedShape();
            foreach (Point point in shapeOne.shape.pointsByAngle.Values) {
                shape.shape.addPoint(point);
            }
            foreach (Point point in shapeTwo.shape.pointsByAngle.Values)
            {
                shape.shape.addPoint(point);
            }
            shape.weight = (1.0 + shapeOne.weight + shapeTwo.weight) / 3.0;
            return shape;
        }
    }
}
