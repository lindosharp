﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Aggregate two weighted shapes into one. */
    interface Aggregator
    {
        WeightedShape aggregate(WeightedShape shapeOne, WeightedShape shapeTwo);
    }
}
