﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FormsTooling._2D;

/** Bridge scout to x,y coordinates. */
namespace _2DTest.lindo
{
    class ScoutBridge : Scout
    {
        public PointCoordinate origin = null;

        public void addBeams(List<DistanceHistogrammBeam> beams, PointCoordinate position, double rotation)
        {
            Shape shape = new Shape();
            foreach (DistanceHistogrammBeam beam in beams)
            {
                if (null != beam && null != beam.P1 && null != beam.P2)
                {
                    shape.addPoint(Transformator.getPoint(beam.P2Error.X - beam.P1.X, beam.P2Error.Y - beam.P1.Y));
                }
            }
            Motion motion = null == origin || null == position ? null : new Motion(Transformator.getPoint(origin.X - position.X, origin.Y - position.Y), rotation);
            origin = position;
            addShape(shape, motion);
        }

        /** Draw shape. */
        public void draw(List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands)
        {
            if (null == drawCommands || null == shapeAll || null == origin)
            {
                return;
            }
            foreach (Point point in shapeAll.shape.pointsByAngle.Values)
            {
                if (null != point)
                {
                    PointCoordinate coordinate = new PointCoordinate(origin.X + Transformator.getX(point), origin.Y + Transformator.getY(point));
                    drawCommands.Add(new FormsTooling._2D.Drawing.DrawPoint(coordinate, Color.Green));
                }
            }
        }

        /** Get shape as (x,y) coordinates. */
        public List<PointCoordinate> getCoordinates()
        {
            List<PointCoordinate> coordinates = new List<PointCoordinate>();
            if (null == shapeAll || null == origin)
            {
                return coordinates;
            }
            foreach (Point point in shapeAll.shape.pointsByAngle.Values)
            {
                if (null != point)
                {
                    coordinates.Add(new PointCoordinate(origin.X + Transformator.getX(point), origin.Y + Transformator.getY(point)));
                }
            }
            return coordinates;
        }
    }
}
