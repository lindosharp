﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Return motion as the only variation (no uncertainty, exact world). */
    class VariatorOne : Variator
    {
        public List<Motion> vary(Motion motion)
        {
            List<Motion> motions = new List<Motion>();
            if (null != motion)
            {
                motions.Add(motion);
            }
            return motions;
        }
    }
}
