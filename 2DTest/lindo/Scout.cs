﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** Collect information about our surroundings and tell where we are. */
    class Scout
    {
        public Aggregator aggregator = new AggregatorThree();
        public Comparator comparator = new ComparatorOne();
        public WeightedShape shapeAll = null;
        public WeightedShape shapeLast = null;
        public Shape shapePoi = new Shape();
        public Variator variator = new VariatorTwo();
        public Motion MotionBest = null;

        public void addShape(Shape shapeOne, Motion motionOne)
        {
            if (null == shapeAll)
            {
                shapeAll = new WeightedShape(shapeOne, 0.333);
            }
            else
            {
                WeightedShape shapeBest = null;
                double weightBest = 0;
                foreach (Motion motionTwo in variator.vary(motionOne))
                {
                    WeightedShape shapeTwo = Transformator.move(shapeAll, motionTwo);
                    double weight = comparator.compare(shapeOne, shapeTwo.shape);
                    if (weight > weightBest)
                    {
                        shapeBest = shapeTwo;
                        weightBest = weight;
                        MotionBest = motionTwo;
                    }
                }
                if (null != shapeBest)
                {
                    shapePoi = Transformator.move(shapePoi, MotionBest);
                    shapeLast = new WeightedShape(shapeOne, weightBest);
                    shapeAll = null == aggregator ? shapeBest : aggregator.aggregate(shapeBest, shapeLast);
                    System.Diagnostics.Debug.WriteLine("Weight:" +  shapeAll.weight);  
                }
            }
        }
    }
}
