﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** 
     * Distribute motions around motion simulating a normal distribution (polar method).
     * Assume that rotation is absolute (compass) and only varies by slight error.
     */
    class VariatorThree : Variator
    {
        public int variationsMax = 5000;

        public List<Motion> vary(Motion motion)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            double u, v, q, p;
            // assume sigma = last motion radius
            double sigmaAngle = null == motion || null == motion.offset ? Math.PI : Math.PI / 4;
            double sigmaRadius = null == motion || null == motion.offset ? 1000.0 : motion.offset.radius;
            double deltaRotation = Math.PI * 2.0 / 180.0; // 2 degrees
            double angle = null == motion || null == motion.offset ? 0.0 : motion.offset.angle;
            double radius = null == motion || null == motion.offset ? 0.0 : motion.offset.radius;
            int variations = null == motion || null == motion.offset ? variationsMax : Math.Min((int)sigmaRadius * 100 + 1, variationsMax);
            List<Motion> motions = new List<Motion>(variations);
            for (int i = 0; i < variations; i++)
            {
                do
                {
                    u = 2.0 * random.NextDouble() - 1.0; // [-1, +1]
                    v = 2.0 * random.NextDouble() - 1.0; // [-1, +1]
                    q = u * u + v * v;
                } while (q == 0.0 || q >= 1);
                p = Math.Sqrt(-2 * Math.Log(q) / q);
                u = u * p;
                v = v * p;
                double angleOne = angle + u * sigmaAngle;
                double radiusOne = radius + v * sigmaRadius;
                double rotationOne = -deltaRotation + 2 * random.NextDouble() * deltaRotation;
                Motion motionOne = new Motion(new Point(angleOne, radiusOne), rotationOne);
                System.Diagnostics.Debug.WriteLine(motionOne);  
                motions.Add(motionOne);
            }
            return motions;
        }
    }
}
