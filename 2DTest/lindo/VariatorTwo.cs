﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** 
     * Distribute motions around motion in a square of size motion.offset.radius / 2.
     * Assume that rotation is absolute (compass) and only varies by slight error.
     */
    class VariatorTwo : Variator
    {
        public int variationsMax = 10000;

        public List<Motion> vary(Motion motion)
        {
            Random random = new Random(DateTime.Now.Millisecond);
            double deltaAngle = Math.PI;
            double deltaRadius = 3000;
            double deltaRotation = Math.PI * 2.5 / 180; // 5 degrees
            double angleMin = - deltaAngle;
            double radiusMin = - deltaRadius;
            if (null != motion && null != motion.offset)
            {
                deltaAngle = Math.Asin(0.25);
                deltaRadius = motion.offset.radius / 4;
                angleMin = motion.offset.angle - deltaAngle;
                radiusMin = motion.offset.radius - deltaRadius;
            }
            int variations = Math.Min((int)deltaRadius * 100 + 1, variationsMax);
            List<Motion> motions = new List<Motion>();
            for (int i = 0; i < variations; i++)
            {
                double angle = angleMin + 2 * random.NextDouble() * deltaAngle;
                double radius = radiusMin + 2 * random.NextDouble() * deltaRadius;
                double rotation = -deltaRotation + 2 * random.NextDouble() * deltaRotation;
                motions.Add(new Motion(new Point(angle, radius), rotation));
            }
            return motions;
        }
    }
}
