﻿using System;
using System.Collections.Generic;
using System.Drawing;
using FormsTooling._2D;

namespace _2DTest.lindo
{
    /** Bridge into Router. */
    class RouterBridge
    {
        public Router routerOne = new RouterOne();
        public Router routerTwo = new RouterTwo();

        public void drawRoutes(List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands, PointCoordinate origin, PointCoordinate target, List<PointCoordinate> points)
        {
            ISet<Route> routes = null == target ? routerOne.findRoutes(origin, target, points) : routerTwo.findRoutes(origin, target, points);
        }

        public void drawRoutes(List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands, ISet<Route> routes)
        {
            foreach (Route route in routes)
            {
                drawRoute(drawCommands, route);
            }
        }

        public void drawRoute(List<FormsTooling._2D.Drawing.IDrawCommand> drawCommands, Route route)
        {
            while (null != route)
            {
                if (null != route.predecessor)
                {
                    PointCoordinate pointOne = new PointCoordinate(route.x, route.y);
                    PointCoordinate pointTwo = new PointCoordinate(route.predecessor.x, route.predecessor.y);
                    drawCommands.Add(new FormsTooling._2D.Drawing.DrawLine(pointOne, pointTwo, Color.Blue));
                }
                route = route.predecessor;
            }
        }
    }
}
