﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DTest.lindo
{
    /** 
     * Add points with higher weight first, add point with lower weight only if
     * - weight is greater than weightMin.
     * - distance to neighbours is greater than distanceMin.
     */
    class AggregatorTwo : Aggregator
    {
        public double weightMin = 0.5;
        public double distanceMinSquare = 400;

        public WeightedShape aggregate(WeightedShape shapeOne, WeightedShape shapeTwo)
        {
            WeightedShape shape = new WeightedShape();
            WeightedShape shapeFirst = shapeOne.weight >= shapeTwo.weight ? shapeOne : shapeTwo;
            foreach (Point point in shapeFirst.shape.pointsByAngle.Values)
            {
                if (!isTooClose(point, shape.shape))
                {
                    shape.shape.addPoint(point);
                }
            }
            shape.weight = shapeFirst.weight;
            WeightedShape shapeLast = shapeOne.weight < shapeTwo.weight ? shapeOne : shapeTwo;
            if (shapeLast.weight > weightMin)
            {
                foreach (Point point in shapeLast.shape.pointsByAngle.Values)
                {
                    if (!isTooClose(point, shape.shape))
                    {
                        shape.shape.addPoint(point);
                    }
                }
                shape.weight = (1.0 + shapeOne.weight + shapeTwo.weight) / 3.0;
            }
            return shape;
        }

        /** Is pointOne too close to shape? */
        public bool isTooClose(Point pointOne, Shape shape)
        {
            foreach (Point pointTwo in shape.pointsOrdered)
            {
                // linear approximation
                double deltaAngle = (pointTwo.angle - pointOne.angle) * pointOne.radius;
                double deltaRadius = pointTwo.radius - pointOne.radius;
                double distanceSquare = deltaAngle * deltaAngle + deltaRadius * deltaRadius;
                if (distanceSquare < distanceMinSquare)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
