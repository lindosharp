﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FormsTooling._2D;
using FormsTooling.Randomize; 

namespace _2DTest
{
    public class World : TrigonBase, IBotsWorld
    {
        public PolygonBase OuterPolygon;
        public List<PolygonBase> InnerPolygons = new List<PolygonBase>();
        public List<MapRasterPoint> Raster = new List<MapRasterPoint>()  ;
        public WorldSettings worldSettings = null;
        public int Rows;
        public int Cols;




        public World(WorldSettings worldsetngs)
        {
            worldSettings = worldsetngs;          
        }

        public int PolygonEdgePoints
        {
            get
            {
                int rval = OuterPolygon.points.Length;
                foreach (PolygonBase poly in InnerPolygons)
                {
                    rval += poly.points.Length;
                }
                return rval;
            }
        }

        public int RasterPoints = 0;
        public bool CreateRaster(BeamSettings beamSetngs)
        {

            if (OuterPolygon == null)
                return false;

            Raster.Clear();

            if (worldSettings.CompareActive == false)
                return false;

            double rasterResolution = worldSettings.Raster.Resolution; 
            double MaxX = OuterPolygon.MaxX;
            double MaxY = OuterPolygon.MaxY;
            Rows = (int)System.Math.Floor(MaxY / rasterResolution);
            Cols = (int)System.Math.Floor(MaxX / rasterResolution);

            
            int LastTriangleIndex = 0;

            DistanceHistogrammCompare comparer = new DistanceHistogrammCompare(100, (worldSettings.Raster.Optimize == OptimizeRasterMode.Significant ? DistanceHistogrammCompareMode.Significants: DistanceHistogrammCompareMode.Distances) );    

            for (int row = 0; row <= Rows; row++)
            {
                for (int col = 0; col <= Cols; col++)
                {
                    double x = col * rasterResolution;
                    double y = row * rasterResolution;
                    if (PointInMap(x, y, LastTriangleIndex))
                    {
                        DistanceHistogramm histogramm = CreateDistanceHistogramm(new PointCoordinate(x, y), beamSetngs);
                        bool bAdd = false;
                        if (worldSettings.Raster.Optimize == OptimizeRasterMode.None)
                            bAdd = true;
                        else
                        { 
                            List<MapRasterPoint> neighbours = FindNeighbours(x,y);
                            if (neighbours.Count == 0)
                                bAdd = true;
                            else
                            {
                                bAdd = true;

                                foreach (MapRasterPoint neighbour in neighbours)
                                {
                                    if (comparer.Compare(histogramm, neighbour.Distances) < 0.3)
                                    {
                                        bAdd = false;
                                        break;
                                    }
                                }
                            
                            
                            
                            }
                        
                        
                        
                        }
                        
                        if (bAdd)
                        {
                            MapRasterPoint rasterPoint = new MapRasterPoint(x, y);
                            rasterPoint.Distances = histogramm;
                            Raster.Add(rasterPoint);
                            RasterPoints++;
                        }
                    }

                }
            }


            return true;
        }

        public List<MapRasterPoint> FindNeighbours(double x, double y)
        {
            return FindNeighbours(x, y, worldSettings.Raster.MaxNeighbourhoodRadius);
        }


        public List<MapRasterPoint> FindNeighbours(double x, double y, double radius)
        {
            List<MapRasterPoint> rval = new List<MapRasterPoint>();
            foreach (MapRasterPoint rasterPoint in Raster)
            {
                if ((rasterPoint.X != x) || (rasterPoint.Y != y))
                {
                    if (DistanceBetweenPoints(new PointCoordinate(x, y), new PointCoordinate(rasterPoint.X, rasterPoint.Y)) <= radius)
                        rval.Add(rasterPoint);
                }

            }
            return rval;
        }

        public DistanceHistogramm CreateDistanceHistogramm(PointCoordinate pt, BeamSettings beamSettings)
        {
            return CreateDistanceHistogramm(pt, 0, beamSettings);         
        }


        public DistanceHistogramm CreateDistanceHistogramm(PointCoordinate pt, int startAngleDegree, BeamSettings beamSettings)
        {
            double startAngle = startAngleDegree * System.Math.PI / 180;
            return CreateDistanceHistogramm(pt, startAngle, beamSettings); 
        }


        public DistanceHistogramm CreateSubjectiveDistanceHistogramm(PointCoordinate pt, double startAngle, BeamSettings beamSettings)
        {
            return CreateDistanceHistogramm(pt, startAngle, beamSettings).Normalize();   
        }
        
        public DistanceHistogramm CreateDistanceHistogramm(PointCoordinate pt, double startAngle, BeamSettings beamSettings)
        {
            Beam beam = new Beam(pt, 0,beamSettings.Range);
            DistanceHistogramm histogramm = new DistanceHistogramm( beamSettings.AngleResolution, beamSettings.Range);
            int histogrammBeamIndex = 0;

            for (int i = 0; i < 360 ; i += beamSettings.AngleResolution)
            {
                double angleRad = (i * 2 * System.Math.PI) / 360;
                beam.Angle = angleRad  + startAngle;

                PointCoordinate ptIntersection = BeamIntersection(beam);
                double distance = beam.Range;
                if (ptIntersection != null) //!!! In real life: Remove Noise from Beams with max Range so that it is invariant
                {
                    distance = TrigonBase.DistanceBetweenPoints(beam.P1, ptIntersection);
                    distance += distance * Randomizer.Random(-beamSettings.RelativeError, beamSettings.RelativeError);   
                    distance += Randomizer.Random (-beamSettings.AbsoluteError, beamSettings.AbsoluteError);   
                }
                DistanceHistogrammBeam histogrammBeam = new DistanceHistogrammBeam(pt, ptIntersection, distance, beam.Angle, histogrammBeamIndex, histogramm );
                histogramm.Beams.Add(histogrammBeam);  
                histogrammBeamIndex++;
            }
            histogramm.CalculateSignificant(); 
            return histogramm;         
        
        }


        public bool PointInMap(double x, double y, int LastTriangleIndex = 0xFFFF)
        {
            bool rval = false;
            if (OuterPolygon.PointInInnerPolygon(x, y, ref LastTriangleIndex))
            { 

                rval = true;
                foreach (PolygonBase innerPolygon in InnerPolygons)
                {
                    int dummy = 0xFFFF;
                    if (innerPolygon.PointInInnerPolygon(x, y, ref  dummy) == true)
                    {
                        rval = false;
                        break;
                    }
                }
            
            
            }

            return rval;
        }

        public PointCoordinate BeamIntersection(Beam beam)
        {
            Beam beamTest = new Beam(beam.P1,beam.Angle, beam.Range);
            PointCoordinate rval = beam.PolygonIntersection(OuterPolygon);
            if (rval != null)
                beamTest.Range = DistanceBetweenPoints(beam.P1, rval);


            foreach (PolygonBase innerPolygon in InnerPolygons)
            {
                PointCoordinate pt = beamTest.PolygonIntersection(innerPolygon);
                if (pt != null)
                {
                    double distance = DistanceBetweenPoints(beam.P1, pt);
                    if (distance < beamTest.Range)
                    {
                        rval = pt;
                        beamTest.Range = distance;
                    }

                }
            }

            return rval;
        
        }



        public List<MapRasterPoint> SelectMatches(DistanceHistogramm histogramm)
        {
            List<MapRasterPoint> rval = new List<MapRasterPoint>();
            WorldRasterCompareSettings compareSettings = worldSettings.Comparer;  
            DistanceHistogrammCompare Comparer = new DistanceHistogrammCompare(compareSettings.AbsoluteDistanceMatch, compareSettings.Mode);

            foreach (MapRasterPoint rasterPoint in Raster)
            {
                DistanceHistogramm comparePoint = rasterPoint.Distances;
                double result = Comparer.Compare(comparePoint, histogramm );
                if (result < compareSettings.CompareMatch)
                {
                    rasterPoint.DrawObject.Marked = true;
                    rval.Add(rasterPoint);
                }
                else
                {
                    rasterPoint.DrawObject.Marked = false;
                }
            }
            return rval;        
        }

    }
}
