﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormsTooling._2D;
using FormsTooling;
using _2DTest.lindo;

namespace _2DTest
{
    public partial class Form1 : Form
    {
        TheWorld world= null;
        DrawObject drawlines = new DrawObject();
        SimulationSettings settings = null;
        Bot bot = null;
        RouterBridge router = new RouterBridge();
        ScoutBridge scoutRecord = new ScoutBridge();
        ScoutBridge scoutPlay = new ScoutBridge();


        public Form1()
        {
            InitializeComponent();
            Initialize("");
        }



        private void Initialize(string SettingsFileName)
        {
            drawContainer1.Objects.Clear();
            drawContainer1.Refresh();
  
            DebugTiming.TimeMeasure("Start", true);
            SetupSettings(SettingsFileName);
            SetupWorld();
            SetupBot();
            SetupRoute();
            SetupDraw();
            DebugTiming.TimeMeasure("End");
        }

        DrawObject raster = new DrawObject();

        private void SetupDraw()
        {
            drawContainer1.Objects.Clear();
            drawContainer1.Objects.Add(bot);
            drawContainer1.Objects.Add(world.OuterPolygon);
            foreach (PolygonBase polygon in world.InnerPolygons)
                drawContainer1.Objects.Add(polygon);

            raster = new DrawObject(); 
            drawContainer1.Objects.Add(raster);
            beams = new DrawObject(); 
            drawContainer1.Objects.Add(beams);
            drawlines= new DrawObject(); 
            drawContainer1.Objects.Add(drawlines);
            RedrawSimulation();
        }
        private void SetupBot()
        {
            bot = new Bot(settings.Bot, world);
            bot.DriveCommandProgress += new EventHandler<DriveCommandProgressEventArgs>(bot_DriveCommandProgress);
            bot.DrivePosition = settings.Bot.StartPosition;
            bot.RealPosition  = settings.Bot.StartPosition;

        }

        #region settings
        private void SetupSettings(string fileName)
        {
            if (fileName == "")
                settings = new SimulationSettings();
            else
                settings = SimulationSettings.Load(fileName);  
            settings.Recalc += new EventHandler<EventArgs>(settings_Recalc);
            settings.Redraw += new EventHandler<EventArgs>(settings_Redraw);
            pg.SelectedObject = settings;
            pg.ExpandAllGridItems(); 
        }

        void settings_Redraw(object sender, EventArgs e)
        {
            RedrawSimulation();
        }


        void RedrawSimulation()
        {
            drawContainer1.Zoom = settings.Draw.Zoom;
            drawContainer1.Refresh();
        }
        void settings_Recalc(object sender, EventArgs e)
        {
            SetupWorld();
        }

        #endregion

        private void SetupWorld()
        {
            world = new TheWorld(settings.World);
            DebugTiming.TimeMeasure("", true);
            System.Diagnostics.Debug.WriteLine(world.PolygonEdgePoints.ToString());
            
            world.CreateRaster(settings.Bot.Beams);
            raster.DrawCommands.Clear(); 
            foreach (MapRasterPoint rasterPoint in world.Raster)
            {
                FormsTooling._2D.Drawing.DrawPoint pt = rasterPoint.DrawObject; // new FormsTooling._2D.Drawing.DrawPoint(new PointCoordinate(rasterPoint.X, rasterPoint.Y), Color.Red);
                raster.DrawCommands.Add(pt);
            }

        }


        Route route = null;
        private void SetupRoute()
        {
            RouteSettings routeSettings = new RouteSettings();
            routeSettings.BufferDepth = 20;
            routeSettings.MinDepth  = 30;
            routeSettings.NeighbourHoodDistance = 150;
            route = new Route(routeSettings);
        }


        private void drawContainer1_MouseDown(object sender, MouseEventArgs e)
        {
            handleMouseEvent(sender, e);
            //drawContainer1_MouseMove(sender, e);
        }


        int LastTriangleMouseIndex = 0;
        DrawObject beams = new DrawObject(); 

        private void handleMouseEvent(object sender, MouseEventArgs e)
        {

            double X = e.X / drawContainer1.Zoom;
            double Y = e.Y / drawContainer1.Zoom;

            //***Set Cursor
            Cursor NewCursor = Cursors.Arrow;
            if (world != null)
            {

                if (world.PointInMap(X, Y, LastTriangleMouseIndex))
                    NewCursor = Cursors.Cross;

            }
            drawContainer1.Cursor = NewCursor;
            //***\\Set Cursor

            beams.DrawCommands.Clear();

            if (world.PointInMap(X, Y, LastTriangleMouseIndex))
            {
                //Generate and draw Histogramm
                DistanceHistogramm thisPoint = world.CreateDistanceHistogramm(new PointCoordinate(X, Y), settings.Bot.Beams);
                DebugTiming.TimeMeasure("Compare", true);

                List<MapRasterPoint> matches = world.SelectMatches(thisPoint);

                lblCompareResult.Text = matches.Count.ToString() + "/" + world.Raster.Count.ToString() + "  " + DebugTiming.TimeMeasure("Compare:");
                lblCompareResult.Refresh();
                if (matches.Count == 0)
                    System.Diagnostics.Debug.WriteLine("STOP");

                ResetChart();

                List<PointCoordinate> distanceList = new List<PointCoordinate>();
                List<PointCoordinate> SignificantList = new List<PointCoordinate>();

                for (int i = 0; i < thisPoint.Beams.Count; i++)
                {
                    DistanceHistogrammBeam distbeam = thisPoint.Beams[i];
                    if ((distbeam.P2 != null) && settings.Draw.ShowBeams)
                    {
                        beams.DrawCommands.Add(new FormsTooling._2D.Drawing.DrawLine(distbeam.P1, distbeam.P2, Color.Red));
                    }
                    distanceList.Add(new PointCoordinate(distbeam.Angle * 180 / System.Math.PI, distbeam.Distance));
                }

                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    // record
                    scoutRecord.variator = new VariatorOne();
                    scoutRecord.addBeams(thisPoint.Beams, new PointCoordinate(X, Y), 0);
                    scoutRecord.draw(beams.DrawCommands);
                    scoutPlay.origin = null;
                    scoutPlay.shapeAll = scoutRecord.shapeAll;
                }
                else
                {
                    // play
                    scoutPlay.aggregator = null;
                    scoutPlay.addBeams(thisPoint.Beams, new PointCoordinate(X, Y), 0);
                    scoutPlay.draw(beams.DrawCommands);
                }
                AddCurve(distanceList, X.ToString("0.00") + ";" + Y.ToString("0.00"), Color.Black, System.Windows.Forms.DataVisualization.Charting.AxisType.Primary, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line);

                for (int i = 0; i < thisPoint.Significant.Count; i++)
                {
                    SignificantSign significant = thisPoint.Significant[i];
                    SignificantList.Add(new PointCoordinate(significant.Angle * 180 / System.Math.PI, significant.Value));
                }

                AddCurve(SignificantList, "Significant", Color.Blue, System.Windows.Forms.DataVisualization.Charting.AxisType.Primary, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick);
            }

            drawContainer1.Refresh();
            chart1.Update();
            Application.DoEvents();

        }




        #region Curves in Chart
        private void ResetChart()
        {
            chart1.Series.Clear();
            chart1.ResetAutoValues();
        }


        private void AddCurve(List<PointCoordinate> data, string name, Color color, System.Windows.Forms.DataVisualization.Charting.AxisType axisType, System.Windows.Forms.DataVisualization.Charting.SeriesChartType chartType)
        { 
            //***series initialization
            System.Windows.Forms.DataVisualization.Charting.Series series = new System.Windows.Forms.DataVisualization.Charting.Series();

            series.ChartType = chartType;

            series.LegendText = name;
            series.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series.Color = color;
            series.YAxisType = axisType;

            for (int i = 0; i < data.Count; i++)
            {
                System.Windows.Forms.DataVisualization.Charting.DataPoint dp = new System.Windows.Forms.DataVisualization.Charting.DataPoint(data[i].X , data[i].Y);
                series.Points.Add(dp);
            }
            chart1.Series.Add(series);
        }
        #endregion

        private void btnTest_Click(object sender, EventArgs e)
        {
            beams.DrawCommands.Clear();
            // set target to latest left click or (750, 750) relative to scout origin.
            PointCoordinate target = null == scoutRecord.origin ? new PointCoordinate(750, 750) : new PointCoordinate(0, 0);
            scoutRecord.shapePoi.clear();
            scoutRecord.shapePoi.addPoint(Transformator.getPoint(target));
            // synch scout with bot (set scout.origin to bot.DrivePosition).
            scoutRecord.variator = new VariatorTwo();
            DistanceHistogramm hist = bot.GetDistanceHistogramm();
            scoutRecord.addBeams(hist.Beams, bot.DrivePosition.Point, 0);
            // route from here to target until we are as close as possible
            while (true)
            {
                PointCoordinate origin = bot.DrivePosition.Point;
                target = Transformator.plus(Transformator.getPoint(scoutRecord.shapePoi.pointsOrdered.First()), scoutRecord.origin);
                //Console.WriteLine("find routes from (" + origin.X + ", " + origin.Y + ") to (" + target.X + ", " + target.Y + ")");
                ISet<lindo.Route> routes = router.routerTwo.findRoutes(origin, target, scoutRecord.getCoordinates());
                //Console.WriteLine("found " + routes.Count + " routes");
                router.drawRoutes(beams.DrawCommands, routes);
                drawContainer1.Refresh();
                if (1 != routes.Count || null == routes.First().predecessor)
                {
                    break;
                }
                PointCoordinate point = bot.DrivePosition.Point;
                lindo.Point polar = new lindo.Point(bot.DrivePosition.Angle, 1);
                lindo.Route route = routes.First();
                route = Transformator.revert(route);
                List<Motion> motions = new List<Motion>();
                for (int i = 0; i < 3 && null != route; i++)
                {
                    //Console.WriteLine("at " + route);
                    lindo.Point polarNext = Transformator.getPoint(route.x - point.X, route.y - point.Y);
                    Motion motion = new Motion(0, polarNext.radius, polarNext.angle - polar.angle);
                    motions.Add(motion);
                    point = new PointCoordinate(route.x, route.y);
                    polar = polarNext;
                    route = route.predecessor;
                }
                handleMotions(motions);
            }
            scoutRecord.draw(beams.DrawCommands);
            drawContainer1.Refresh();
        }

        private void handleMotions(List<Motion> motions) {
            foreach (Motion motion in motions)
            {
                DriveVector LastPosition = bot.DrivePosition;
                handleMotion(bot, motion);
                DistanceHistogramm histogramAngle0 = bot.GetDistanceHistogramm().Transform(new PointCoordinate(0, 0), -bot.DrivePosition.Angle);
                scoutRecord.addBeams(histogramAngle0.Beams, bot.DrivePosition.Point, 0);
                Motion motionBest = scoutRecord.MotionBest;
                if (motionBest != null)
                {
                    bot.DrivePosition.Point = Transformator.minus(LastPosition.Point, Transformator.getPoint(motionBest.offset));
                    bot.DrivePosition.Angle = bot.DrivePosition.Angle - motionBest.rotation;
                    System.Diagnostics.Debug.WriteLine(bot.PosCompare());
                }
                //UpdatePosInfos(bot.GetDistanceHistogramm());
            }
        }

        private void handleMotion(Bot bot, Motion motion)
        {
            if (null == bot || null == motion)
            {
                return;
            }
            if (0 != motion.rotation)
            {
                bot.TurnCommand(motion.rotation);
            }
            if (0 != motion.offset.radius)
            {
                bot.DriveCommand((int)motion.offset.radius, DriveCommandDirection.Forward);
            }
        }

        private void btnFreeCorridor_Click(object sender, EventArgs e)
        {
            bot.Escape();
            UpdatePosInfos(bot.GetDistanceHistogramm());
        }

        void bot_DriveCommandProgress(object sender, DriveCommandProgressEventArgs e)
        {
            RedrawSimulation(); 
        }

        void UpdatePosInfos(DistanceHistogramm histogramm)
        {
            //Matches in raster
            List<MapRasterPoint> matches = world.SelectMatches(histogramm);

            
            
            //Find lines
            drawlines.DrawCommands.Clear();
            HistogrammLineFinder.AngleResolution = settings.Bot.Beams.AngleResolution;
            HistogrammLineFinder finder = new HistogrammLineFinder();
            List<PlausibleLine> lines = finder.TestHistogramm(histogramm, settings.Bot.Beams.AngleResolution);

            foreach (PlausibleLine ln in lines)
            {
                PointCoordinate P1 = ln.values[0].P2;
                PointCoordinate P2 = ln.values[ln.Width - 1].P2;
                if (P1 != null && P2 != null)
                {
                    FormsTooling._2D.Drawing.DrawLine drawLine = new FormsTooling._2D.Drawing.DrawLine(ln.values[0].P2Error, ln.values[ln.Width - 1].P2Error, Color.Yellow, 5);
                    drawlines.DrawCommands.Add(drawLine);
                }
            }

            //beams.DrawCommands.Clear(); 
            for (int i = 0; i < histogramm.Beams.Count; i++)
            {
                DistanceHistogrammBeam distbeam = histogramm.Beams[i];
                if ((distbeam.P2 != null) && settings.Draw.ShowBeams)
                    beams.DrawCommands.Add(new FormsTooling._2D.Drawing.DrawLine(distbeam.P1, distbeam.P2, Color.Red));
            }

            RedrawSimulation();
        
        
        }

        private void tsLoadSettings_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "(*.sim)| *.sim";
            dlg.InitialDirectory = AppPath;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Initialize(dlg.FileName);
            }
        }

        private void tsSaveSettings_Click(object sender, EventArgs e)
        {
            string AppPath = Application.StartupPath;
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "(*.sim)| *.sim";
            dlg.InitialDirectory = AppPath;
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                settings.Save(dlg.FileName);
            }

        }

        private void btnExplore_Click(object sender, EventArgs e)
        {
            scoutRecord.variator = new VariatorTwo();
            AggregatorThree agg = new AggregatorThree();
            agg.limit = 200;
            scoutRecord.aggregator = agg;

            bot.WallDrive(15000, null, null); //Leerfahrt
            bot.WallDrive(15000, scoutRecord, null);

        }


    }
}
