﻿namespace _2DTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lblCompareResult = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.drawContainer1 = new FormsTooling._2D.Drawing.DrawContainer();
            this.btnFreeCorridor = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.pg = new System.Windows.Forms.PropertyGrid();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsLoadSettings = new System.Windows.Forms.ToolStripButton();
            this.tsSaveSettings = new System.Windows.Forms.ToolStripButton();
            this.btnExplore = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(3, 510);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(1014, 153);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // lblCompareResult
            // 
            this.lblCompareResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCompareResult.AutoSize = true;
            this.lblCompareResult.Location = new System.Drawing.Point(3, 454);
            this.lblCompareResult.Name = "lblCompareResult";
            this.lblCompareResult.Size = new System.Drawing.Size(35, 13);
            this.lblCompareResult.TabIndex = 12;
            this.lblCompareResult.Text = "label1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.drawContainer1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnExplore);
            this.splitContainer1.Panel2.Controls.Add(this.btnFreeCorridor);
            this.splitContainer1.Panel2.Controls.Add(this.btnTest);
            this.splitContainer1.Panel2.Controls.Add(this.pg);
            this.splitContainer1.Panel2.Controls.Add(this.lblCompareResult);
            this.splitContainer1.Size = new System.Drawing.Size(1014, 476);
            this.splitContainer1.SplitterDistance = 771;
            this.splitContainer1.TabIndex = 16;
            // 
            // drawContainer1
            // 
            this.drawContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drawContainer1.Location = new System.Drawing.Point(0, 0);
            this.drawContainer1.Name = "drawContainer1";
            this.drawContainer1.Size = new System.Drawing.Size(771, 476);
            this.drawContainer1.TabIndex = 5;
            this.drawContainer1.X = 0D;
            this.drawContainer1.Y = 0D;
            this.drawContainer1.Zoom = 0.15D;
            this.drawContainer1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.drawContainer1_MouseDown);
            // 
            // btnFreeCorridor
            // 
            this.btnFreeCorridor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnFreeCorridor.Location = new System.Drawing.Point(131, 409);
            this.btnFreeCorridor.Name = "btnFreeCorridor";
            this.btnFreeCorridor.Size = new System.Drawing.Size(105, 27);
            this.btnFreeCorridor.TabIndex = 14;
            this.btnFreeCorridor.Text = "FreeCorridor";
            this.btnFreeCorridor.UseVisualStyleBackColor = true;
            this.btnFreeCorridor.Click += new System.EventHandler(this.btnFreeCorridor_Click);
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTest.Location = new System.Drawing.Point(77, 409);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(48, 27);
            this.btnTest.TabIndex = 13;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // pg
            // 
            this.pg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pg.Location = new System.Drawing.Point(2, 3);
            this.pg.Name = "pg";
            this.pg.Size = new System.Drawing.Size(234, 400);
            this.pg.TabIndex = 0;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLoadSettings,
            this.tsSaveSettings});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1020, 25);
            this.toolStrip1.TabIndex = 17;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsLoadSettings
            // 
            this.tsLoadSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsLoadSettings.Image = ((System.Drawing.Image)(resources.GetObject("tsLoadSettings.Image")));
            this.tsLoadSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsLoadSettings.Name = "tsLoadSettings";
            this.tsLoadSettings.Size = new System.Drawing.Size(23, 22);
            this.tsLoadSettings.Text = "Load Settings";
            this.tsLoadSettings.Click += new System.EventHandler(this.tsLoadSettings_Click);
            // 
            // tsSaveSettings
            // 
            this.tsSaveSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSaveSettings.Image = ((System.Drawing.Image)(resources.GetObject("tsSaveSettings.Image")));
            this.tsSaveSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSaveSettings.Name = "tsSaveSettings";
            this.tsSaveSettings.Size = new System.Drawing.Size(23, 22);
            this.tsSaveSettings.Text = "Save Settings";
            this.tsSaveSettings.Click += new System.EventHandler(this.tsSaveSettings_Click);
            // 
            // btnExplore
            // 
            this.btnExplore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnExplore.Location = new System.Drawing.Point(6, 409);
            this.btnExplore.Name = "btnExplore";
            this.btnExplore.Size = new System.Drawing.Size(65, 27);
            this.btnExplore.TabIndex = 15;
            this.btnExplore.Text = "Explore";
            this.btnExplore.UseVisualStyleBackColor = true;
            this.btnExplore.Click += new System.EventHandler(this.btnExplore_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 664);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.chart1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "2D planar sensor simulation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label lblCompareResult;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private FormsTooling._2D.Drawing.DrawContainer drawContainer1;
        private System.Windows.Forms.PropertyGrid pg;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnFreeCorridor;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsLoadSettings;
        private System.Windows.Forms.ToolStripButton tsSaveSettings;
        private System.Windows.Forms.Button btnExplore;
    }
}

