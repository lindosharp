﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using FormsTooling._2D;
using System.Xml.Serialization;

namespace _2DTest
{
    public class SimulationSettings : SimulationSettingsBase 
    {
        public SimulationSettings()
        {
            Draw = new DrawSettings();
            Draw.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            Draw.Redraw += new EventHandler<EventArgs>(Property_Redraw);

            Bot = new BotSettings();
            Bot.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            Bot.Redraw += new EventHandler<EventArgs>(Property_Redraw);

            World = new WorldSettings();
            World.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            World.Redraw += new EventHandler<EventArgs>(Property_Redraw);
        }

        public DrawSettings Draw { get; set; }
        public BotSettings Bot { get; set; }
        public WorldSettings World { get; set; }

    }













    public class SimulationSettingsBase
    {
        public event EventHandler<EventArgs> Recalc;
        public event EventHandler<EventArgs> Redraw;


        protected void RaiseRecalc()
        {
            if (Recalc != null)
                Recalc(this, new EventArgs());
        }

        protected void RaiseRedraw()
        {
            if (Redraw != null)
                Redraw(this, new EventArgs());
        }

        protected void Property_Redraw(object sender, EventArgs e)
        {
            RaiseRedraw();
        }

        protected void Property_Recalc(object sender, EventArgs e)
        {
            RaiseRecalc();
        }


        public static SimulationSettings Load(string FileName)
        {
            try
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(SimulationSettings) );
                System.IO.FileStream fs = new System.IO.FileStream(FileName, System.IO.FileMode.Open);
                System.Xml.XmlReader xr =  System.Xml.XmlReader.Create(fs);
                SimulationSettings o = (SimulationSettings) mySerializer.Deserialize(xr);
                fs.Flush();
                fs.Close();
                fs = null;

                
                return o;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        public bool Save(string fileName)
        {
            try
            {
                XmlSerializer mySerializer = new XmlSerializer(typeof(SimulationSettings));
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                System.Xml.XmlWriterSettings xmlsettings = new System.Xml.XmlWriterSettings();
                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create);
                xmlsettings.Encoding = System.Text.Encoding.UTF8;
                System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(fs, xmlsettings);
                mySerializer.Serialize(xw, this);
                fs.Flush();
                fs.Close();
                fs = null;
                
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
