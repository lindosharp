﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace _2DTest
{
    public enum OptimizeRasterMode
    {
        None = 0,
        Distance = 1,
        Significant = 2
    }

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class WorldRasterSettings : SimulationSettingsBase
    {
        private int resolution = 100;
        public int Resolution
        {
            get { return resolution; }

            set
            {
                resolution = value;
                RaiseRecalc();
            }
        }


        private OptimizeRasterMode optimize = OptimizeRasterMode.None;
        public OptimizeRasterMode Optimize
        {
            get { return optimize; }

            set
            {
                optimize = value;
                RaiseRecalc();
            }
        }


        private double maxNeighbourhoodRadius = 100;
        public double MaxNeighbourhoodRadius
        {
            get { return maxNeighbourhoodRadius; }

            set
            {
                maxNeighbourhoodRadius = value;
                if (optimize != OptimizeRasterMode.None)
                    RaiseRecalc();
            }
        }
    }

}
