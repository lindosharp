﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class DrawSettings : SimulationSettingsBase
    {
        private double zoom = 0.22;
        public double Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                RaiseRedraw();
            }
        }
        public bool ShowBeams { get; set; }
    }
}
