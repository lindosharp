﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class WorldRasterCompareSettings : SimulationSettingsBase
    {
        public WorldRasterCompareSettings()
        {
            AbsoluteDistanceMatch = 100;
            CompareMatch = 0.3;
            Mode = DistanceHistogrammCompareMode.Distances;
        }
        public double AbsoluteDistanceMatch { get; set; }
        public double CompareMatch { get; set; }
        public DistanceHistogrammCompareMode Mode { get; set; }
        public int DegreeShift { get; set; }
    }

}
