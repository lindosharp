﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class BeamSettings : SimulationSettingsBase
    {
        private int angleResolution = 10;
        public int AngleResolution
        {
            get { return angleResolution; }
            set
            {
                angleResolution = value;
                RaiseRecalc();
            }
        }

        private double range = 1000;
        public double Range
        {
            get { return range; }
            set
            {
                range = value;
                RaiseRecalc();
            }
        }


        private double relativeError = 0.01;
        public double RelativeError
        {
            get { return relativeError; }
            set
            {
                relativeError = value;
                RaiseRecalc();
            }
        }

        private int absoluteError = 2;
        public int AbsoluteError
        {
            get { return absoluteError; }
            set
            {
                absoluteError = value;
                RaiseRecalc();
            }
        }




        private double relativeAngleError = 0;
        public double RelativeAngleError
        {
            get { return relativeAngleError; }
            set
            {
                relativeAngleError = value;
                RaiseRecalc();
            }
        }
    }

}
