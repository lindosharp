﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class WorldSettings : SimulationSettingsBase
    {
        public WorldSettings()
        {
            Comparer = new WorldRasterCompareSettings();
            Raster = new WorldRasterSettings();
            Comparer.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            Raster.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            Comparer.Redraw += new EventHandler<EventArgs>(Property_Redraw);
            Raster.Redraw += new EventHandler<EventArgs>(Property_Redraw);
        }

        bool compareActive = false;
        public bool CompareActive
        {
            get { return compareActive;} 
            set
            {
                compareActive = value;
                RaiseRecalc(); 
            }
        
        }
        public WorldRasterCompareSettings Comparer { get; set; }
        public WorldRasterSettings Raster { get; set; }
    }
}
