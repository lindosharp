﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using FormsTooling._2D;
using FormsTooling.Randomize;

namespace _2DTest
{
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class BotSettings : SimulationSettingsBase
    {
        public BotSettings()
        {
            Beams = new BeamSettings();
            Beams.Recalc += new EventHandler<EventArgs>(Property_Recalc);
            Beams.Redraw += new EventHandler<EventArgs>(Property_Redraw);
            StartPosition =  new DriveVector(0, new PointCoordinate(50, 50));
            Diameter = 80;
            WheelDistance = 70;
            CalculationDivider = 50;
            DriveError = new DriveError();
        }

        
        /// <summary>
        /// Settings for the surounding distance sensor
        /// </summary>
        public BeamSettings Beams { get; set; }

        /// <summary>
        /// Diameter of the vehicle (used to calculate if the bot can pass a way)
        /// </summary>
        public double Diameter{ get; set; }

        /// <summary>
        /// Distance between the wheels (used to calculate turn angles)
        /// </summary>
        public double WheelDistance { get; set; }

        /// <summary>
        /// A drive error modell (Difference between wheel diameters, noising from rough plane and slipping at acceleration)
        /// </summary>
        public DriveError DriveError{ get; set; }


        /// <summary>
        /// The start position (Unknown by bot but used to calculate beams)
        /// </summary>
        public DriveVector StartPosition{ get; set; }

        /// <summary>
        /// Segement length used to calculate the movement of the bot (Collision detection)
        /// /// </summary>
        public int CalculationDivider { get; set; }



    }




    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class DriveError
    {
        public DriveError()
        {
            Noise = 0.001;
            AccSlip = 0.02;
            DecSlip = 0.04;
            WheelDiameterDeviation = 0.002;
        }
        [Flags]
        public enum CalcNoisyWaySlipMarker
        { 
            IsMid = 0,
            IsFirst = 1,
            IsLast = 2
        }

        public void CalcNoisyWays(ref double left, ref double right, CalcNoisyWaySlipMarker marker)
        {
            //Static deviation
            left += (left * WheelDiameterDeviation / 2);
            right -= (right * WheelDiameterDeviation / 2);


            //Noise
            if (Noise != 0)
            {
                left += left * Randomizer.Random(-Noise, Noise);
                right += right * Randomizer.Random(-Noise, Noise);
            }

            //Slip at first calculation
            if (marker.HasFlag(CalcNoisyWaySlipMarker.IsFirst))
            {
                left -= left * AccSlip;
                right -= right * AccSlip;
            }

            //Slip at last calculation
            if (marker.HasFlag(CalcNoisyWaySlipMarker.IsLast))
            {
                left += left * DecSlip;
                right += right * DecSlip;
            }
        }


        /// <summary>
        /// Randomized relative deviation per step
        /// </summary>
        public double Noise {get; set;}

        /// <summary>
        /// static relative deviation per step caused by different wheel diameter
        /// </summary>
        public double WheelDiameterDeviation { get; set; }

        /// <summary>
        /// Absolutely randomized Deviation caused by acceleration (drive command start)
        /// </summary>
        public double AccSlip {get; set;}

        /// <summary>
        /// Absolutely randomized Deviation caused by deceleration (drive command end)
        /// </summary>
        public double DecSlip {get; set;}


    }

}
